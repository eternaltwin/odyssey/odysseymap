<?php 
$rights['edit']=false;
header ('Content-type:text/html; charset=utf-8');
include_once('./tools/identification.php');
?>

<!DOCTYPE html>
<html>
    <head>
		<meta charset="utf-8" />
		<!--[if IE ]><meta http-equiv="Content-Type" content="text/html;charset=UTF-8" xml:lang="fr" lang="fr"/><![endif]-->
        <title>OdysseyMap</title>
        <link rel="stylesheet" type="text/css" href="style/default.css" />
		<link href="favicon.png" type="image/x-icon" rel="SHORTCUT ICON">
    </head>
    <body>
	
	<div>Chargement !</div>
	
	<canvas id="map" width="500" height="500">
            Votre navigateur n'est pas assez performant ! Il doit surment dater de quelques années, or on n'arrête pas le progrès et si vous utilisez encore Internet Explorer ou FireFox dans un version inférieure à 4.0 vous passez à côté d'une tonne de fonctionnalité géniales disponible sur Internet ! Pire, vous ralentissez le développement du web qui ne peut pas profiter de toute sa puissance à cause de ces navigateurs.
    </canvas>
	

	
	<div id="details">
		<div id="detailscoord"><div class="tablerow"><span id="coordx"></span><span id="coordy"></span><span id="blockdetails" class="detailsopt"><img alt="" src="./img/other/detailslock.png" /></span><span id="detailswidth" class="detailsopt"><img alt="" src="./img/other/detailswidthon.png" /></span></div></div>
		<h1 id="detailssea"></h1>
		<h2 id="detailsisland"></h2>
		<h3 id="detailscase"></h3>
		<div id="detailsacts"><div class="tablerow"><button id="infgood"><span></span> +1</button><button id="infbad"><span></span> +1</button><button id="imhere">Je suis là</button></div>
		<div class="tablerow"><button id="st_u">Inconnue</button><button id="st_v">Visitée</button><button id="st_e">Vidée</button><button id="st_c">Nettoyée</button></div></div>
		<div id="detailscontent"></div>
		<ul id="detailsedit"></ul>
	</div>
	
	<menu id="topmenu">
		<ul class="top">
			<li>
				<a href="#">Zoom</a>
				<ul class="sub" id="zoomslist">
					<li data-zoom="0.5">x 0,5</li>
					<li data-zoom="0.83333333333333333">Carte du jeu</li>
					<li data-zoom="1">x 1</li>
					<li data-zoom="2">x 2</li>
					<li data-zoom="5">x 5</li>
					<li data-zoom="10">x 10</li>
					<li data-zoom="20">x 20</li>
					<li data-zoom="50">x 50</li>
				</ul>
			</li><li id="gotomenu">
				<a href="#">Aller en...</a>
				<ul class="sub">
					<li id="gotoorigin">Origine</li>
					<li id="gotomypos">Ma position</li>
					<li id="goto" class="notbutton">
						<form style="display:table;width:100%;">
							<div class="tablerow"><span class="tablecell">X </span><span class="tablecell"><input /></span><span class="tablecell"><input style="width:20px;" maxlength="1" /></span><span class="tablecell"><input style="width:20px;" maxlength="1" /></span></div>
							<div class="tablerow"><span class="tablecell">Y </span><span class="tablecell"><input /></span><span class="tablecell"><input style="width:20px;" maxlength="1" /></span><span class="tablecell"><input style="width:20px;" maxlength="1" /></span></div>
						</form>
						<p style="text-align:center;margin:0;padding:0;"><button id="gotocustom">Go</button></p>
					</li>
				</ul>
			</li><li>
				<a href="#">Interface</a>
				<ul class="sub">
					<li>Plus bas niveau d'infos</li>
					<li>Forcer le rechargement</li>
					<li class="notbutton"><form>Colorer...<br />
						<label><input type="radio" name="colorby" value="default" /> Ne pas colorer</label><br/>
						<label><input type="radio" name="colorby" value="progress" /> Progression</label>
					</form></li>
				</ul>
			</li><li>
				<a href="#">Parties</a>
				<ul class="sub">
					<li>Partie 1</li>
					<li>Partie 2</li>
					<li>Partie 3</li>
					<li>Partie 4</li>
				</ul>
			</li><?php if((!isset($user))OR(!$user['name'])OR(!$_SESSION['id'])){ ?><li>
				<a href="#" id="logmenutitle">S'identifier</a>
				<ul class="sub" id="menu_account">
					<li class="notbutton">
						<form action="tools/identification.php" method="get" id="logform">
							<label>UID</label><input name="loguid" id="loguid" /><br/>
							<label>Pubkey</label><input name="logpk"  id="logpk" /><br/>
							<label><input type="checkbox" name="rememberlog" id="rememberlog"/>Retenir mes identifiants.</label><br />
							<input type="submit" id="logsub" value="Envoyer" />
						</form>
					
					</li>
					<li class="notbutton"><a onclick="window.top.location.href='http://muxxu.com/a/odysseymap?act=ids'" href="http://muxxu.com/a/odysseymap?act=ids">Obtenir mes identifiants</a></li>
				</ul>
			</li><?php }else{?><li>
				<a href="#"><?php echo $user['name'];?></a>
				<ul class="sub" id="menu_account">
					<li>Se déconnecter</li>
					<li class="notbutton"><a onclick="window.top.location.href='http://muxxu.com/a/odysseymap?act=ids'" href="http://muxxu.com/a/odysseymap?act=ids">Obtenir mes identifiants</a></li>
				</ul>
			</li><?php }; ?>
		</ul>
	</menu>

	<?php if(isset($_GET['act']) AND $_GET['act']=='ids'){?>
		<div id="getidswrapper">
			<div id="getids">
			<?php if(isset($_GET['uid']) AND isset($_GET['pubkey'])){?>
				<p>Vos identifiants sont :</p>
				<dl>
					<div>
						<dt>UID</dt><dd><?php echo $_GET['uid'] ;?></dd>
					</div>
					<div>
						<dt>Pubkey</dt><dd><?php echo $_GET['pubkey'] ;?></dd>
					</div>
				</dl>
			<?php }else{ ?>
				<p>Impossible de retrouver les identifiants.</p>
			<?php }?>
				<p id="hasbutton"><button onclick="$('#getidswrapper').css('display','none')">Fermer</button></p>
			</div>
		</div>
	<?php }?>
    </body>

	
	<?php if(isset($user)){

		?>
			<script>
				var logged=true;
				var opt={'edit':<?php echo $rights['edit'] ? 'true' : 'false';?>,'smallinf':true,'curpart':<?php echo $_SESSION['part'];?>};
				var parts=[null,<?php echo $user['part1'].','.$user['part2'].','.$user['part3'].','.$user['part4'];?>];
			</script>
		<?php
		
	
	}else{
	
		?>
			<script>
				var logged=false;
			</script>
		<?php
	}
	
	?>
	

	
	
	
	<script src="script/jquery.js"></script>
	<script src="script/odysseymap.js"></script>
</html>