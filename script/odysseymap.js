
/**
 * 
 * credits for this plugin go to brandonaaron.net
 * 	
 * unfortunately his site is down
 * 
 * @param {Object} up
 * @param {Object} down
 * @param {Object} preventDefault
 */
jQuery.fn.extend({
	mousewheel: function(up, down, preventDefault) {
		return this.hover(
			function() {
				jQuery.event.mousewheel.giveFocus(this, up, down, preventDefault);
			},
			function() {
				jQuery.event.mousewheel.removeFocus(this);
			}
		);
	},
	mousewheeldown: function(fn, preventDefault) {
		return this.mousewheel(function(){}, fn, preventDefault);
	},
	mousewheelup: function(fn, preventDefault) {
		return this.mousewheel(fn, function(){}, preventDefault);
	},
	unmousewheel: function() {
		return this.each(function() {
			jQuery(this).unmouseover().unmouseout();
			jQuery.event.mousewheel.removeFocus(this);
		});
	},
	unmousewheeldown: jQuery.fn.unmousewheel,
	unmousewheelup: jQuery.fn.unmousewheel
});


jQuery.event.mousewheel = {
	giveFocus: function(el, up, down, preventDefault) {
		if (el._handleMousewheel) jQuery(el).unmousewheel();
		
		if (preventDefault == window.undefined && down && down.constructor != Function) {
			preventDefault = down;
			down = null;
		}
		
		el._handleMousewheel = function(event) {
			if (!event) event = window.event;
			if (preventDefault)
				if (event.preventDefault) event.preventDefault();
				else event.returnValue = false;
			var delta = 0;
			if (event.wheelDelta) {
				delta = event.wheelDelta/120;
				if (window.opera) delta = -delta;
			} else if (event.detail) {
				delta = -event.detail/3;
			}
			if (up && (delta > 0 || !down))
				up.apply(el, [event, delta]);
			else if (down && delta < 0)
				down.apply(el, [event, delta]);
		};
		
		if (window.addEventListener)
			window.addEventListener('DOMMouseScroll', el._handleMousewheel, false);
		window.onmousewheel = document.onmousewheel = el._handleMousewheel;
	},
	
	removeFocus: function(el) {
		if (!el._handleMousewheel) return;
		
		if (window.removeEventListener)
			window.removeEventListener('DOMMouseScroll', el._handleMousewheel, false);
		window.onmousewheel = document.onmousewheel = null;
		el._handleMousewheel = null;
	}
};



//fonctions
function type(v){//renvoie le type formaté d'une variable
	return (typeof(v)).toLowerCase();};
function isChildOf(child,parent){//Teste la parentée d'un élément
	if(child===parent){return true;}while(child=child.parentNode){if(child===parent){return true;}}return false;}
function inArray(a,k){//Teste la présence d'un objet dans un array
	for(var i=0,c=a.length;i<c;i++){if(a[i]==k){return true;}}return false;}
function clone(o){//Clone un objet
	if(typeof(o) != 'object' || o == null){return o;}var n = o.constructor();for(var i in o){n[i] = clone(o[i]);}return n;}
function px(v,b){//Ajuste au pixel(false) ou demi-pixel(true).
	if(b){return v%1==0?v+0.5:0.5+v-(v%1);}else{return v%1==0?v:v-(v%1);}}
function setCookie(n,v,t){
	var e=new Date();e.setDate(e.getDate() + t);var v=escape(v)+((t==null) ? "" : "; expires="+e.toUTCString()+";path=/");document.cookie=n+"="+v;}
function getCookie(n){
	var i,x,y,a=document.cookie.split(";");for (i=0;i<a.length;i++){x=a[i].substr(0,a[i].indexOf("="));y=a[i].substr(a[i].indexOf("=")+1);x=x.replace(/^\s+|\s+$/g,"");if (x==n){return unescape(y);}}}
	
window.onload=function(){
	//ressources
	var res={
		'item':['',
			{'img':new Image(),'src':'./img/items/plate.png','n':'Planche'},
			{'img':new Image(),'src':'./img/items/cane1.png','n':'Canne du Débutant'},
			{'img':new Image(),'src':'./img/items/cane2.png','n':'Canne Intermédiaire'},
			{'img':new Image(),'src':'./img/items/epi.png','n':'Épi de Blé'},
			{'img':new Image(),'src':'./img/items/lotos.png','n':'Feuilles de Lotos'},
			{'img':new Image(),'src':'./img/items/moly.png','n':'Racines de Moly'},
			{'img':new Image(),'src':'./img/items/zlance.png','n':'Lance de Zeus'},
			{'img':new Image(),'src':'./img/items/triton.png','n':'Conque des Tritons'},
			{'img':new Image(),'src':'./img/items/zpendant.png','n':'Pendentif de Zeus'},
			{'img':new Image(),'src':'./img/items/poem.png','n':'Page de Poème'},
			{'img':new Image(),'src':'./img/items/kmedal.png','n':'Médaille de Kabal'},
			{'img':new Image(),'src':'./img/items/hmap.png','n':'Carte des Mines'},
			{'img':new Image(),'src':'./img/items/coton.png','n':'Balle de Coton'},
			{'img':new Image(),'src':'./img/items/sail.png','n':'Voile Antiphon'},
			{'img':new Image(),'src':'./img/items/crane.png','n':'Crâne de Ptéros'},
			{'img':new Image(),'src':'./img/items/savmap.png','n':'Carte de la Mer Sauvage'},
			{'img':new Image(),'src':'./img/items/blaze.png','n':'Blason d\'Halberti'},
			{'img':new Image(),'src':'./img/items/rose.png','n':'Rose d\'Airain'},
			{'img':new Image(),'src':'./img/items/ribon.png','n':'Ruban de Flora'},
			{'img':new Image(),'src':'./img/items/stones.png','n':'Petits Cailloux'}
		],
		'mob':['',
			{'n':'Blob'},
			{'n':'Blob puissant'},
			{'n':'Blob féroce'},
			{'n':'Bulot maudit'},
			{'n':'Araignée géante'},
			{'n':'Renard laineux'},
			{'n':'Serpent venimeux'},
			{'n':'Foudrine'},
			{'n':'Pygmé incrédule'},
			{'n':'Galupiote'},
			{'n':'Apprenti'},
			{'n':'Squelette vengeur	'},
			{'n':'Ame en peine'},
			{'n':'Frisquette'},
			{'n':'Cyclope de Tartan'},
			{'n':'Golem d\'argile'},
			{'n':'Diablotin sournois'},
			{'n':'Panthère'},
			{'n':'Poisillon'},
			{'n':'Scarabée géant'},
			{'n':'Picounet'},
			{'n':'Fulgor'},
			{'n':'Homme lézard'},
			{'n':'Guêpe géante'},
			{'n':'Armanite Puante'},
			{'n':'Mouskito Family'},
			{'n':'Sado-gorgogne'},
			{'n':'Tape-Taupe'},
			{'n':'Rototo'},
			{'n':'Seigneur Cobra'},
			{'n':'Sorcier du Crépuscule'},
			{'n':'Sorcier de l\'Aurore'},
			{'n':'Raide Momie'},
			{'n':'Chauve-chat farceur'},
			{'n':'Shaman-poussière'},
			{'n':'Minotaure'},
			{'n':'Kevinathan Biblique'}
		],
		'build':{
			'albc':{'img':new Image(),'src':'./img/build/alambic.png'},
			'autel':{'img':new Image(),'src':'./img/build/autel.png'},
			'bibli':{'img':new Image(),'src':'./img/build/bibli.png'},
			'btl':{'img':new Image(),'src':'./img/build/btl.png'},
			'chst':{'img':new Image(),'src':'./img/build/tchst.png'},
			'empt':{'img':new Image(),'src':'./img/build/empt.png'},
			'food':{'img':new Image(),'src':'./img/build/food.png'},
			'ftn':{'img':new Image(),'src':'./img/build/fontaine.png'},
			'gld1':{'img':new Image(),'src':'./img/build/gld1.png'},
			'gld2':{'img':new Image(),'src':'./img/build/gld2.png'},
			'gld3':{'img':new Image(),'src':'./img/build/gld3.png'},
			'homa':{'img':new Image(),'src':'./img/build/homa.png'},
			'homb':{'img':new Image(),'src':'./img/build/homb.png'},
			'homc':{'img':new Image(),'src':'./img/build/homc.png'},
			'homd':{'img':new Image(),'src':'./img/build/homd.png'},
			'inn':{'img':new Image(),'src':'./img/build/inn.png'},
			'port':{'img':new Image(),'src':'./img/build/port100.png'},
			'rns':{'img':new Image(),'src':'./img/build/rns.png'},
			'rum':{'img':new Image(),'src':'./img/build/rum.png'},
			'tbl':{'img':new Image(),'src':'./img/build/table.png'},
			'tpl':{'img':new Image(),'src':'./img/build/temple.png'},
			'vdra':{'img':new Image(),'src':'./img/build/vdra.png'},
			'vdrb':{'img':new Image(),'src':'./img/build/vdrb.png'}
		},
		'hero':['',
			{'img':new Image(),'src':'./img/heroes/heratus.png','n':'Heratus'},
			{'img':new Image(),'src':'./img/heroes/celeide.png','n':'Celeïde'},
			{'img':new Image(),'src':'./img/heroes/gloria.png','n':'Gloria'},
			{'img':new Image(),'src':'./img/heroes/dolskin.png','n':'Dolskin'},
			{'img':new Image(),'src':'./img/heroes/tiber.png','n':'Tiber'},
			{'img':new Image(),'src':'./img/heroes/epivone.png','n':'Epivone'},
			{'img':new Image(),'src':'./img/heroes/horas.png','n':'Horas'},
			{'img':new Image(),'src':'./img/heroes/stirenx.png','n':'Stirenx'},
			{'img':new Image(),'src':'./img/heroes/torkish.png','n':'Torkish'},
			{'img':new Image(),'src':'./img/heroes/antones.png','n':'Antones'},
			{'img':new Image(),'src':'./img/heroes/egoine.png','n':'Egoine'},
			{'img':new Image(),'src':'./img/heroes/maugrine.png','n':'Maugrine'},
			{'img':new Image(),'src':'./img/heroes/espiroth.png','n':'Espiroth'},
			{'img':new Image(),'src':'./img/heroes/tasulys.png','n':'Tasulys'},
			{'img':new Image(),'src':'./img/heroes/keperi.png','n':'Keperi'},
			{'img':new Image(),'src':'./img/heroes/tatsi.png','n':'Tatsi'},
		],
		'other':{
			'boat':{'img':new Image(),'src':'./img/other/boat.png'},
			'tpx':{'img':new Image(),'src':'./img/tpx.png'},
			'mapcomp':{'img':new Image(),'src':'./img/other/mapcomp.png'},
			'nsp':{'img':new Image(),'src':'./img/other/bignsp.png'},
			'pion':{'img':new Image(),'src':'./img/other/pion.svg'}
		}
	}
	var lt=['item','build','hero','other'];//lt->LoadThat, la boucle ci-dessous charge les images en associant l'attribut src à un objet Image
	for(var i=0,c=lt.length;i<c;i++){
		for(var j in res[lt[i]]){
			if(typeof(res[lt[i]][j])=='object'){res[lt[i]][j].img.src=res[lt[i]][j].src;}}}
	//Initialisation de canvas
	
	var map = document.getElementById('map');
	if(!map){alert("Impossible de récupérer le canvas");return;}
    var ctxt = map.getContext('2d');
	if(!ctxt){alert("Impossible de récupérer le context du canvas");return;}
	//Protypage de la méthode innerBoxShadow de Canvas
	if(!CanvasRenderingContext2D.prototype.innerBoxShadow){
		CanvasRenderingContext2D.prototype.innerBoxShadow=function(x,y,w,h,c,s){
			var ctxt = this;
			ctxt.strokeStyle='rgba('+c[0]+','+c[1]+','+c[2]+','+c[3]+')';
			ctxt.lineWidth=1;
			x=px(x,true);
			y=px(y,true);
			for(var i=0;i<=s;i++){
				ctxt.strokeRect(x+i,y+i,w-(2*i),h-(2*i));
				ctxt.strokeStyle='rgba('+c[0]+','+c[1]+','+c[2]+','+c[3]*(1-i/s)+')';
			}			
		}
	}

	ctxt.size={'w':ctxt.canvas.width,'h':ctxt.canvas.height,'x':function(v){return 0.01*v*ctxt.canvas.width;},'y':function(v){return 0.01*v*ctxt.canvas.height;}};
	ctxt.resize={};
	ctxt.resize.full=function(){var s={'y':ctxt.canvas.offsetWidth,'x':ctxt.canvas.offsetHeight};ctxt.canvas.width=s.y;ctxt.canvas.height=s.x;ctxt.size.w=ctxt.canvas.width;ctxt.h=ctxt.canvas.height;ctxt.size.w=ctxt.canvas.width;ctxt.size.h=ctxt.canvas.height;omap.center={'x':parseInt(ctxt.size.x(50)),'y':parseInt(ctxt.size.y(50))};};
	//On est prêts à travailler.
	var omap={'seas':[],'islands':[]};//Objet global.
	omap.center={'x':parseInt(ctxt.size.x(50)),'y':parseInt(ctxt.size.y(50))};
	omap.enverg={};
	omap.opos={}
	omap.ccoord={'x':0,'y':0,'xx':1,'yy':0};

	var draw={};

	draw.bg=function(){
		ctxt.fillStyle='rgb(71,61,47)';
		ctxt.fillRect(0,0,ctxt.size.w,ctxt.size.h);
	}
	draw.quadr=function(){
		ctxt.strokeStyle='rgba(0,0,0,0.1)';
		if(zoom>=20){ctxt.lineWidth = 5;}
		else if(zoom>=5){ctxt.lineWidth = 3;}
		else{ctxt.lineWidth = 1;}
		
		for(var x=px((omap.center.x+omap.opos.x),true)%(18*zoom);x<ctxt.size.w;x+=(18*zoom)){
			ctxt.beginPath();
			ctxt.moveTo(x,0);
			ctxt.lineTo(x,ctxt.size.h);
			ctxt.stroke();
			ctxt.closePath();}
		for(var y=px((omap.center.y+omap.opos.y),true)%(18*zoom);y<ctxt.size.h;y+=(18*zoom)){
			ctxt.beginPath();
			ctxt.moveTo(0,y);
			ctxt.lineTo(ctxt.size.w,y);
			ctxt.stroke();
			ctxt.closePath();}
		if(zoom>=5){
			if(zoom>=20){ctxt.lineWidth = 3;}
			else{ctxt.lineWidth = 1;}
			
			for(var x=px((omap.center.x+omap.opos.x),true)%(6*zoom);x<ctxt.size.w;x+=(6*zoom)){
				ctxt.beginPath();
				ctxt.moveTo(x,0);
				ctxt.lineTo(x,ctxt.size.h);
				ctxt.stroke();
				ctxt.closePath();}
			for(var y=px((omap.center.y+omap.opos.y),true)%(6*zoom);y<ctxt.size.h;y+=(6*zoom)){
				ctxt.beginPath();
				ctxt.moveTo(0,y);
				ctxt.lineTo(ctxt.size.w,y);
				ctxt.stroke();
				ctxt.closePath();}
		}
		if(zoom>=20){
			ctxt.lineWidth = 1;
			for(var x=px((omap.center.x+omap.opos.x),true)%(zoom);x<ctxt.size.w;x+=(zoom)){
				ctxt.beginPath();
				ctxt.moveTo(x,0);
				ctxt.lineTo(x,ctxt.size.h);
				ctxt.stroke();
				ctxt.closePath();}
			for(var y=px((omap.center.y+omap.opos.y),true)%(zoom);y<ctxt.size.h;y+=(zoom)){
				ctxt.beginPath();
				ctxt.moveTo(0,y);
				ctxt.lineTo(ctxt.size.w,y);
				ctxt.stroke();
				ctxt.closePath();}
		}
	}
	draw.seas=function(){
		for(var id=0,c=omap.seas.length;id<c;id++){
			if(omap.seas[id]!=null){
				var sea=omap.seas[id];
				if((sea.top.x>=omap.enverg.top.x&&sea.top.x<=omap.enverg.btm.x)||(sea.btm.x>=omap.enverg.top.x&&sea.btm.x<=omap.enverg.btm.x)||(sea.top.y>=omap.enverg.top.y&&sea.top.y<=omap.enverg.btm.y)||(sea.btm.y>=omap.enverg.top.y&&sea.btm.y<=omap.enverg.btm.y)||(sea.top.x<=omap.enverg.top.x&&sea.btm.x>=omap.enverg.btm.x)||(sea.top.y<=omap.enverg.top.y&&sea.btm.y>=omap.enverg.btm.y)){//Teste si la mer est dans l'écran.
					var coord1=cg.coord2rpx(sea.top.x,sea.top.y,sea.top.xx,sea.top.yy);
					var coord2=cg.ncoord2rpx(sea.btm.x,sea.btm.y,sea.btm.xx,sea.btm.yy);
					ctxt.fillStyle=get.seaColor(sea);
					ctxt.fillRect(coord1.x,coord1.y,coord2.x-coord1.x,coord2.y-coord1.y);
					draw.islands(sea);
					ctxt.innerBoxShadow(coord1.x,coord1.y,coord2.x-coord1.x-1,coord2.y-coord1.y-1,[0,0,0,0.3],1.5*zoom);
				}
			}
		}
	}
	draw.islands=function(sea){
		if(zoom>=5/6&&sea.islands){
			for(var id=0,c=sea.islands.length;id<c;id++){
				var island=omap.islands[sea.islands[id]];
				if(island!=null && ((island.top.x>=omap.enverg.top.x&&island.top.x<=omap.enverg.btm.x)||(island.btm.x>=omap.enverg.top.x&&island.btm.x<=omap.enverg.btm.x)||(island.top.y>=omap.enverg.top.y&&island.top.y<=omap.enverg.btm.y)||(island.btm.y>=omap.enverg.top.y&&island.btm.y<=omap.enverg.btm.y)||(island.top.x<=omap.enverg.top.x&&island.btm.x>=omap.enverg.btm.x)||(island.top.y<=omap.enverg.top.y&&island.btm.y>=omap.enverg.btm.y))){//Teste si l'île est dans l'écran.
					var coor1=cg.coord2rpx(island.top.x,island.top.y,island.top.xx,island.top.yy);
					var coor2=cg.ncoord2rpx(island.btm.x,island.btm.y,island.btm.xx,island.btm.yy);
					ctxt.fillStyle=get.islandColor(island);//A changer
					if(zoom>=20&&island.cases){//on dessine les cases
						draw.cases(island);
					}else if(zoom>=2 && (omap.edit.editing!='i'||(omap.edit.editing=='i'&&island.id!=omap.edit.cur.i.id))){//on dessine les arrondis
						ctxt.beginPath();
						ctxt.arc(coor1.x+(6/4)*zoom,coor1.y+(6/4)*zoom,(6/8)*zoom,Math.PI*1.5,Math.PI,true);
						ctxt.lineTo(coor1.x+(6/8)*zoom,coor2.y-(6/4)*zoom);
						ctxt.arc(coor1.x+(6/4)*zoom,coor2.y-(6/4)*zoom,(6/8)*zoom,Math.PI,Math.PI*0.5,true);
						ctxt.lineTo(coor2.x-(6/4)*zoom,coor2.y-(6/8)*zoom);
						ctxt.arc(coor2.x-(6/4)*zoom,coor2.y-(6/4)*zoom,(6/8)*zoom,Math.PI*0.5,0,true);
						ctxt.lineTo(coor2.x-(6/8)*zoom,coor1.y+(6/4)*zoom);
						ctxt.arc(coor2.x-(6/4)*zoom,coor1.y+(6/4)*zoom,(6/8)*zoom,0,Math.PI*1.5,true);
						ctxt.closePath();
						ctxt.fill();
					}else if(omap.edit.editing!='i'||(omap.edit.editing=='i'&&island.id!=omap.edit.cur.i.id)){//on dessine des rectangles
						ctxt.fillRect(coor1.x,coor1.y,coor2.x-coor1.x,coor2.y-coor1.y);
					}
				}
			}
		}
	}
	draw.cases=function(island){
		var coordit={'x':island.top.x,'y':island.top.y,'xx':island.top.xx,'yy':island.top.yy};//Coordonnées du coin supérieur de l'île
		var size=get.islandSize(island);
			for(var i=0,c=size.y;i<c;i++){
				for(var j=0,cc=size.x;j<cc;j++){
					var lieu = island.cases[i][j];
					if(lieu!=0&&lieu!="0"){
						var coord=cg.addCoords(coordit,{'x':0,'y':0,'xx':0,'yy':0,'xxx':j,'yyy':i});
						var coora=cg.coord2rpx(coord.x,coord.y,coord.xx,coord.yy,coord.xxx,coord.yyy);
						var coorb=cg.ncoord2rpx(coord.x,coord.y,coord.xx,coord.yy,coord.xxx,coord.yyy);
						ctxt.fillStyle=get.islandColor(island);
						var cnct=[
							!(i==0||island.cases[i-1][j]==0),//haut
							!(i==island.cases.length-1||island.cases[i+1][j]==0),//bas
							!(j==0||island.cases[i][j-1]==0),//gauche
							!(j==island.cases[i].length-1||island.cases[i][j+1]==0),//droite
							!(i==0||j==0||island.cases[i-1][j-1]==0),//haut-gauche
							!(i==0||j==island.cases[i].length-1||island.cases[i-1][j+1]==0),//haut-droite
							!(i==island.cases.length-1||j==0||island.cases[i+1][j-1]==0),//bas-gauche
							!(i==island.cases.length-1||j==island.cases[i].length-1||island.cases[i+1][j+1]==0)//bas-droite
						];
						ctxt.beginPath();//on commence en haut à gauche
						if(cnct[0]){ctxt.moveTo(coora.x+zoom/6,coora.y);}//ligne du haut
						else if(!cnct[2]){ctxt.moveTo(coora.x+zoom/3,coora.y+zoom/6);}
						else{ctxt.moveTo(coora.x+zoom/6,coora.y+zoom/6);}
						if (cnct[0]&&cnct[2]&&cnct[4]){ctxt.lineTo(coora.x,coora.y);ctxt.lineTo(coora.x,coora.y+zoom/6);}//angle en haut à gauche
						else if(cnct[0]&&(!cnct[2])){ctxt.lineTo(coora.x+zoom/6,coora.y+zoom/6);}
						else if((!cnct[0])&&cnct[2]){ctxt.lineTo(coora.x,coora.y+zoom/6);}
						else if(cnct[0]&&cnct[2]&&(!cnct[4])){ctxt.arc(coora.x,coora.y,zoom/6,0,Math.PI*0.5,false);}
						else{ctxt.arc(coora.x+zoom/3,coora.y+zoom/3,zoom/6,Math.PI*1.5,Math.PI,true);}
						if(cnct[2]){ctxt.lineTo(coora.x,coorb.y-zoom/6);}//ligne de gauche
						else{ctxt.lineTo(coora.x+zoom/6,coorb.y-zoom/6);}
						if (cnct[2]&&cnct[1]&&cnct[6]){ctxt.lineTo(coora.x,coorb.y);ctxt.lineTo(coora.x+zoom/6,coorb.y);}//angle en bas à gauche
						else if(cnct[2]&&(!cnct[1])){ctxt.lineTo(coora.x+zoom/6,coorb.y-zoom/6);}
						else if((!cnct[2])&&cnct[1]){ctxt.lineTo(coora.x+zoom/6,coorb.y);}
						else if(cnct[2]&&cnct[1]&&(!cnct[6])){ctxt.arc(coora.x,coorb.y,zoom/6,Math.PI*1.5,0,false);}
						else{ctxt.arc(coora.x+zoom/3,coorb.y-zoom/3,zoom/6,Math.PI*1,Math.PI*0.5,true);}
						if(cnct[1]){ctxt.lineTo(coorb.x-zoom/6,coorb.y);}//ligne du bas
						else{ctxt.lineTo(coorb.x-zoom/6,coorb.y-zoom/6);}
						if (cnct[1]&&cnct[3]&&cnct[7]){ctxt.lineTo(coorb.x,coorb.y);ctxt.lineTo(coorb.x,coorb.y-zoom/6);}//angle en bas à droite
						else if(cnct[1]&&(!cnct[3])){ctxt.lineTo(coorb.x-zoom/6,coorb.y-zoom/6);}
						else if((!cnct[1])&&cnct[3]){ctxt.lineTo(coorb.x,coorb.y-zoom/6);}
						else if(cnct[1]&&cnct[3]&&(!cnct[7])){ctxt.arc(coorb.x,coorb.y,zoom/6,Math.PI,Math.PI*1.5,false);}
						else{ctxt.arc(coorb.x-zoom/3,coorb.y-zoom/3,zoom/6,Math.PI*0.5,0,true);}
						if(cnct[3]){ctxt.lineTo(coorb.x,coora.y+zoom/6);}//ligne de droite
						else{ctxt.lineTo(coorb.x-zoom/6,coora.y+zoom/6);}
						if (cnct[3]&&cnct[0]&&cnct[5]){ctxt.lineTo(coorb.x,coora.y);ctxt.lineTo(coorb.x-zoom/6,coora.y);}//angle en haut à droite
						else if(cnct[3]&&(!cnct[0])){ctxt.lineTo(coorb.x-zoom/6,coora.y+zoom/6);}
						else if((!cnct[3])&&cnct[0]){ctxt.lineTo(coorb.x-zoom/6,coora.y);}
						else if(cnct[3]&&cnct[0]&&(!cnct[5])){ctxt.arc(coorb.x,coora.y,zoom/6,Math.PI*0.5,Math.PI,false);}
						else{ctxt.arc(coorb.x-zoom/3,coora.y+zoom/3,zoom/6,0,Math.PI*1.5,true);}
						ctxt.closePath();
						ctxt.fill();
					}
				}
			}
			for(var i=0,c=size.y;i<c;i++){
				for(var j=0,cc=size.x;j<cc;j++){
					var lieu = island.cases[i][j];
					if(lieu!=0&&lieu!="0"){
					var coord=cg.addCoords(coordit,{'x':0,'y':0,'xx':0,'yy':0,'xxx':j,'yyy':i});
					var mcoord=cg.mcoord2rpx(coord.x,coord.y,coord.xx,coord.yy,coord.xxx,coord.yyy);
					ctxt.lineWidth=0.07*zoom;//chemins
					ctxt.strokeStyle='rgba(0,0,0,0.5)';
					if(lieu.pr>0){
						ctxt.beginPath();
						ctxt.moveTo(px(mcoord.x),px(mcoord.y));
						ctxt.lineTo(px(mcoord.x+zoom*lieu.pr),px(mcoord.y));
						ctxt.stroke();}
					if(lieu.pb>0){
						ctxt.beginPath();
						ctxt.moveTo(px(mcoord.x),px(mcoord.y));
						ctxt.lineTo(px(mcoord.x),px(mcoord.y+zoom*lieu.pb));
						ctxt.stroke();}

					if(lieu.s){
						var img,params={'sw':50,'sh':50,'dx':0.25,'dy':0.25,'cw':0.5,'ch':0.5};
						switch(lieu.s){
							case 'albc': case 'autel': case 'bibli': case 'btl': case 'chst': case 'empt': case 'food': case 'ftn': case 'homa': case 'homb': case 'homc': case 'homd': case 'inn': case 'rns': case 'rum': case 'tbl': case 'tpl': case 'vdra': case 'vdrb':
								img=res.build[lieu.s].img;
							break;
							case 'gld':
								if(lieu.c<50){img=res.build.gld1.img}
								else if(lieu.c<200){img=res.build.gld2.img}
								else{img=res.build.gld3.img}
							break;
							case 'port':
								img=res.build.port.img;
								params.sw=params.sh=100;
								params.cw=params.ch=1;
								switch(lieu.c){
									case 't':params.dx=0.5;params.dy=0.7;break;
									case 'r':params.dx=0.5;params.dy=0.6;break;
									case 'b':params.dx=0.5;params.dy=0.4;break;
									default :case 'l':params.dx=0.7;params.dy=0.6;break;
								}
							break;
							case 'nsp': img=res.other.nsp.img;params.sw=params.sh=256;break;
							default: img=res.other.tpx.img;params.sw=params.sh=1;break;
						}
						ctxt.drawImage(img, 0, 0, params.sw, params.sh, mcoord.x-params.dx*zoom, mcoord.y-params.dy*zoom, params.cw*zoom, params.ch*zoom);
										
						params={'sw':42,'sh':42,'dx':0.3,'dy':0.3,'cw':0.6,'ch':0.6};
						if((lieu.s=='homa'||lieu.s=='homb'||lieu.s=='homc'||lieu.s=='homd')&&lieu.c.c1=='hero'){//Portrait du héros
							if(lieu.c.c2){
								img=res.hero[parseInt(lieu.c.c2)].img;
								ctxt.drawImage(img, 0, 0, params.sw, params.sh, mcoord.x-params.dx*zoom, mcoord.y-params.dy*zoom, params.cw*zoom, params.ch*zoom);
							}
						}
						params={'sw':44,'sh':44,'dx':0.25,'dy':0.25,'cw':0.5,'ch':0.5};
						if(lieu.s=='chst'){//Contenu du coffre
							switch(lieu.c.c1){
								case'map':img=res.other.mapcomp.img;break;
								case'gld':img=res.build.gld1.img;params.sw=params.sh=50;params.dy=0.2;break;
								case'item':img=res.item[lieu.c.c2].img;break;
								default:img=res.other.tpx.img;params.sw=params.sh=1;
							}
							ctxt.drawImage(img, 0, 0, params.sw, params.sh, mcoord.x-params.dx*zoom, mcoord.y-params.dy*zoom, params.cw*zoom, params.ch*zoom);
						}
						params={'sw':256,'sh':256,'dx':-0.1,'dy':-0.1,'cw':0.4,'ch':0.4};
						if(lieu.c){
							if((lieu.c=='nsp')||(lieu.s=='btl'&&(typeof(lieu.c)).toLowerCase()=='object'&&inArray(lieu.c,'nsp'))||(lieu.s=='rns'&&((lieu.c.c1=='nsp')||((typeof(lieu.c.c1)).toLowerCase()=='object'&&inArray(lieu.c.c1,'nsp')))||(lieu.c.c2=='nsp'))||(lieu.s=='homa'&&((!lieu.c)||lieu.c.nsp=='on'||lieu.c.c1=='nsp'))||(lieu.s=='vdrb'&&((!lieu.c)||lieu.c.nsp=='on'||lieu.c.c1=='nsp'))){
								ctxt.drawImage(img=res.other.nsp.img, 0, 0, params.sw, params.sh, mcoord.x-params.dx*zoom, mcoord.y-params.dy*zoom, params.cw*zoom, params.ch*zoom);
							}
						}
					}
				}
			}
		}
	}
	draw.curpos=function(){
		var pos=get.curPart().pos;
		switch(pos.type){
			case 'i':
				
				if(zoom<=10){
				
					var island=omap.islands[pos.id];
					if(island!=null && ((island.top.x>=omap.enverg.top.x&&island.top.x<=omap.enverg.btm.x)||(island.btm.x>=omap.enverg.top.x&&island.btm.x<=omap.enverg.btm.x)||(island.top.y>=omap.enverg.top.y&&island.top.y<=omap.enverg.btm.y)||(island.btm.y>=omap.enverg.top.y&&island.btm.y<=omap.enverg.btm.y)||(island.top.x<=omap.enverg.top.x&&island.btm.x>=omap.enverg.btm.x)||(island.top.y<=omap.enverg.top.y&&island.btm.y>=omap.enverg.btm.y))){
						var pos1=cg.coord2rpx(island.top.x,island.top.y,island.top.xx,island.top.yy),
							pos2=cg.coord2rpx(island.btm.x,island.btm.y,island.btm.xx,island.btm.yy),
							cpos={'x':(pos1.x+pos2.x)/(2),'y':(pos1.y+pos2.y)/(2)};

						
						
						ctxt.drawImage(res.other.pion.img, 0, 0, 1000, 1000, cpos.x-2*zoom, cpos.y-5*zoom, 10*zoom, 10*zoom);

					}
				
				}
				
			break;
			case 's':
				if(zoom<=2){
			
					var sea=omap.seas[pos.id];
					if(sea!=null && ((sea.top.x>=omap.enverg.top.x&&sea.top.x<=omap.enverg.btm.x)||(sea.btm.x>=omap.enverg.top.x&&sea.btm.x<=omap.enverg.btm.x)||(sea.top.y>=omap.enverg.top.y&&sea.top.y<=omap.enverg.btm.y)||(sea.btm.y>=omap.enverg.top.y&&sea.btm.y<=omap.enverg.btm.y)||(sea.top.x<=omap.enverg.top.x&&sea.btm.x>=omap.enverg.btm.x)||(sea.top.y<=omap.enverg.top.y&&sea.btm.y>=omap.enverg.btm.y))){
						var pos1=cg.coord2rpx(sea.top.x,sea.top.y,sea.top.xx,sea.top.yy),
							pos2=cg.coord2rpx(sea.btm.x,sea.btm.y,sea.btm.xx,sea.btm.yy),
							cpos={'x':(pos1.x+pos2.x)/(2),'y':(pos1.y+pos2.y)/(2)};
						
						ctxt.drawImage(res.other.boat.img, 0, 0, 52, 52, cpos.x-7*zoom, cpos.y-15*zoom, 20*zoom, 20*zoom);
					}
			
				}
			
			break;
		
		}
	}
	
	
	draw.redraw=function(){
		ctxt.clearRect(0,0,ctxt.size.w,ctxt.size.h);
		draw.bg();
		draw.seas();
		if(omap.edit.editing){
			omap.edit.draw();
		}
		draw.quadr();
		draw.curpos();
	}
	
	cg={};//CanvasGraph
	cg.rpx2px=function(x,y){
		return{'x':x-omap.center.x-omap.opos.x,'y':y-omap.center.y-omap.opos.y}}
	cg.px2rpx=function(x,y){
		return{'x':x+omap.center.x+omap.opos.x,'y':y+omap.center.y+omap.opos.y}}
	cg.px2coord=function(x,y,b){
		var coord={};
		if(!b){
			x=Math.floor(x/zoom);
			y=Math.floor(y/zoom);
		}
		coord.x=Math.floor(x/18);
		coord.y=Math.floor(y/18);
		coord.xx=Math.floor((x-coord.x*18)/6);
		coord.yy=Math.floor((y-coord.y*18)/6);
		coord.xxx=(x-coord.x*18)%6;
		coord.yyy=(y-coord.y*18)%6;
		return coord;	
	}
	cg.rpx2coord=function(x,y){
		return cg.px2coord(x-omap.center.x-omap.opos.x,y-omap.center.y-omap.opos.y);
	}
	cg.coord2px=function(x,y,xx,yy,xxx,yyy,b){
		var coord={};
		if(typeof(x)=='number'&&typeof(y)=='number'){
			coord.x=18*x;
			coord.y=18*y;
			if(typeof(xx)=='number'&&typeof(yy)=='number'){
				coord.x+=6*xx;
				coord.y+=6*yy;
				if(typeof(xxx)=='number'&&typeof(yyy)=='number'){
					coord.x+=xxx;
					coord.y+=yyy;
				}
			}
		}
		if(!b){
			coord.x=parseInt(coord.x*zoom);
			coord.y=parseInt(coord.y*zoom);
		}
		return coord;
	}
	cg.coord2rpx=function(x,y,xx,yy,xxx,yyy){
		var coord=cg.coord2px(x,y,xx,yy,xxx,yyy);
		coord.x=coord.x+omap.opos.x+omap.center.x;
		coord.y=coord.y+omap.opos.y+omap.center.y;
		return coord;
	}
	cg.mcoord2rpx=function(x,y,xx,yy,xxx,yyy){
		var npos=cg.coord2rpx(x,y,xx,yy,xxx,yyy), npos2=cg.ncoord2rpx(x,y,xx,yy,xxx,yyy);
		return {'x':(npos.x+npos2.x)/(2),'y':(npos.y+npos2.y)/(2)};
	}
	cg.mcoord2opos=function(x,y,xx,yy,xxx,yyy){
		var npos=cg.coord2px(x,y,xx,yy,xxx,yyy);
		if(typeof(xxx)=='number'&&typeof(yyy)=='number'){
			var npos2=cg.coord2px(x,y,xx,yy,xxx+1,yyy+1);
		}else if(typeof(xx)=='number'&&typeof(yy)=='number'){
			var npos2=cg.coord2px(x,y,xx+1,yy+1,xxx,yyy);
		}else if(typeof(x)=='number'&&typeof(y)=='number'){
			var npos2=cg.coord2px(x+1,y+1,xx,yy,xxx,yyy);
		}
		omap.opos={'x':(npos.x+npos2.x)/(-2),'y':(npos.y+npos2.y)/(-2)};
	}
	cg.addCoords=function(c1,c2){
		var c3={}
			if(typeof(c2.xxx)!='number'&&typeof(c2.yyy)!='number'){c2.xxx=c2.yyy=0;}
			if(typeof(c2.xx)!='number'&&typeof(c2.yy)!='number'){c2.xx=c2.yy=0;}
			if(typeof(c2.x)!='number'&&typeof(c2.y)!='number'){c2.x=c2.y=0;}
			
			c1=cg.coord2px(c1.x,c1.y,c1.xx,c1.yy,c1.xxx,c1.yyy);
			c2=cg.coord2px(c2.x,c2.y,c2.xx,c2.yy,c2.xxx,c2.yyy);
			
			c3=cg.px2coord(c1.x+c2.x,c1.y+c2.y);
			
		return c3;
	}
	cg.lcoord2rpx=function(x,y,xx,yy,xxx,yyy){
		px=cg.ncoord2rpx(x,y,xx,yy,xxx,yyy)
		px.x-=1;
		px.y-=1;
		return px;
	}
	cg.ncoord2rpx=function(x,y,xx,yy,xxx,yyy){
		var coord={'x':x,'y':y,'xx':xx,'yy':yy,'xxx':xxx,'yyy':yyy};
		if(typeof(xxx)=='number'&&typeof(yyy)=='number'){
			var ncoord=cg.addCoords(coord,{'x':0,'y':0,'xx':0,'yy':0,'xxx':1,'yyy':1});
			var px=cg.coord2rpx(ncoord.x,ncoord.y,ncoord.xx,ncoord.yy,ncoord.xxx,ncoord.yyy);
		}else if(typeof(xx)=='number'&&typeof(yy)=='number'){
			var ncoord=cg.addCoords(coord,{'x':0,'y':0,'xx':1,'yy':1});
			var px=cg.coord2rpx(ncoord.x,ncoord.y,ncoord.xx,ncoord.yy);
		}else if(typeof(x)=='number'&&typeof(y)=='number'){
			var ncoord=cg.addCoords(coord,{'x':1,'y':1});
			var px=cg.coord2rpx(ncoord.x,ncoord.y);
		}
		return px;
	}
	cg.mapEnverg=function(){
		var coord1=cg.rpx2coord(0,0), coord2=cg.rpx2coord(ctxt.size.w,ctxt.size.h)
		omap.enverg={'top':coord1,'btm':coord2};
	}

	get={};
	
	get.islandColor=function(i){
		var c;
		if(i.st){
			switch(i.st){
				case 2:c='rgb(213,126,68)';break;
				case 3:c='rgb(225,227,53)';break;
				case 4:c='rgb(97,161,61)';break;
				default:c='rgb(143,125,79)';
			}
		}else{
			c='rgb(143,125,79)';
		}
		return c;
	}
	
	get.seaColor=function(s){
		var c;
		if(s.st){
			switch(s.st){
				case 2:c='rgb(164,96,50)';break;
				case 3:c='rgb(147,148,51)';break;
				case 4:c='rgb(86,125,63)';break;
				default:c='rgb(92,79,56)';
			}
		}else{
			c='rgb(92,79,56)';
		}
		return c;
	}
	
	get.datas=function(f){
		cg.mapEnverg();
		if(zoom>=20){var lvl=3, func=get.lvl3;}
		else if(zoom>=4.9/6){var lvl=2, func=get.lvl2;}
		else{var lvl = 1, func=get.lvl1;}
		$.ajax({
			type: "POST",
			url:'./tools/datas.php',
			data: 'getomaplvl='+lvl+'&x1='+omap.enverg.top.x+'&y1='+omap.enverg.top.y+'&xx1='+omap.enverg.top.xx+'&yy1='+omap.enverg.top.yy+'&x2='+omap.enverg.btm.x+'&y2='+omap.enverg.btm.y+'&xx2='+omap.enverg.btm.xx+'&yy2='+omap.enverg.btm.yy,
			complete:func
		});
	}
	
	get.lvl1=function(r){
		
		var fs=JSON.parse(getCookie('fs')), datas = jQuery.parseJSON(r.responseText);
		for(var id in datas.seas){
			var sea=datas.seas[id];
			for(var c in sea.top){
				sea.top[c]=parseInt(sea.top[c]);
			}for(var c in sea.btm){
				sea.btm[c]=parseInt(sea.btm[c]);
			}
			for(var i=0,c=get.curPart().seas.length;i<c;i++){
				if(get.curPart().seas[i].id==id){sea.st=get.curPart().seas[i].st;};}
			sea.id=id;
			omap.seas[id]=sea;
			if(!inArray(fs,id)){fs.push(parseInt(id));}
		}
		setCookie('fs',JSON.stringify(fs),365*24*3600);
		draw.redraw();
	}
	
	get.lvl2=function(r){
		var fs=JSON.parse(getCookie('fs')), fi=JSON.parse(getCookie('fi')), datas = jQuery.parseJSON(r.responseText);
		for(var id in datas.seas){
			var sea=datas.seas[id];
			for(var c in sea.top){
				sea.top[c]=parseInt(sea.top[c]);
			}for(var c in sea.btm){
				sea.btm[c]=parseInt(sea.btm[c]);
			}
			if(!sea.islands){sea.islands=[];}
			if(logged){
				for(var i=0,c=get.curPart().seas.length;i<c;i++){
					if(get.curPart().seas[i].id==id){sea.st=get.curPart().seas[i].st;};}
			}
			sea.id=id;
			omap.seas[id]=sea;
			if(!inArray(fs,id)){fs.push(parseInt(id));}
		}
		setCookie('fs',JSON.stringify(fs),365*24*3600);
		for(var id in datas.islands){
			var island=datas.islands[id];
			island['sea']=parseInt(island['sea']);
			for(var c in island.top){
				island.top[c]=parseInt(island.top[c]);
			}for(var c in island.btm){
				island.btm[c]=parseInt(island.btm[c]);
			}
			for(var i=0,c=get.curPart().islands.length;i<c;i++){
				if(get.curPart().islands[i].id==id){island.st=get.curPart().islands[i].st;};}
			island.id=id;
			if((omap.seas[island['sea']])&&
			(!inArray(omap.seas[island['sea']].islands,id))
			){
			omap.seas[island['sea']].islands.push(parseInt(id));}
			omap.islands[id]=island;
			if(!inArray(fi,id)){fi.push(parseInt(id));}
		}	
		setCookie('fi',JSON.stringify(fi),365*24*3600);		
		draw.redraw();
	}
	
	get.lvl3=function(r){
		var fs=JSON.parse(getCookie('fs')), fi=JSON.parse(getCookie('fi')), fc=JSON.parse(getCookie('fc')), datas = jQuery.parseJSON(r.responseText);
		for(var id in datas.seas){
			var sea=datas.seas[id];
			for(var c in sea.top){
				sea.top[c]=parseInt(sea.top[c]);
			}for(var c in sea.btm){
				sea.btm[c]=parseInt(sea.btm[c]);
			}
			if(!sea.islands){sea.islands=[];}
			for(var i=0,c=get.curPart().seas.length;i<c;i++){
				if(get.curPart().seas[i].id==id){sea.st=get.curPart().seas[i].st;};}
			sea.id=id;
			omap.seas[id]=sea;
			if(!inArray(fs,id)){fs.push(parseInt(id));}
		}
		setCookie('fs',JSON.stringify(fs),365*24*3600);
		for(var id in datas.islands){
			var island=datas.islands[id];
			island['sea']=parseInt(island['sea']);
			for(var c in island.top){
				island.top[c]=parseInt(island.top[c]);
			}for(var c in island.btm){
				island.btm[c]=parseInt(island.btm[c]);
			}
			if(island['cases']!=''){
				island['cases']=JSON.parse(island['cases']);
				if(!inArray(fc,id)){fc.push(parseInt(id));}
			}
			for(var i=0,c=get.curPart().islands.length;i<c;i++){
				if(get.curPart().islands[i].id==id){island.st=get.curPart().islands[i].st;};}
			island.id=id;
			if(!inArray(omap.seas[island['sea']].islands,id)){omap.seas[island['sea']].islands.push(id);}
			omap.islands[id]=island;
			if(!inArray(fi,id)){fi.push(parseInt(id));}
		}
		setCookie('fi',JSON.stringify(fi),365*24*3600);
		setCookie('fc',JSON.stringify(fc),365*24*3600);
		draw.redraw();
	}
	
	get.elem2coord=function(lvl,x,y){
		switch(lvl){
			case 'i':
				if(type(omap.islands[x])=='object'){
					var i=omap.islands[x];
					return {'x':i.top.x,'y':i.top.y,'xx':i.top.xx,'yy':i.top.yy};
				}else{
					return false;
				}
			break;
			default:
			return false;
		}
	}
	
	get.coord2elem=function(lvl,x,y,xx,yy,xxx,yyy){
		for(var id=0,c=omap.seas.length;id<c;id++){
			if(omap.seas[id]!=null){
				var sea=omap.seas[id];
				if(sea.top.x<=x && x<=sea.btm.x && sea.top.y<=y && y<= sea.btm.y
				&& (sea.top.x!=x||sea.top.xx<=xx) && (sea.btm.x!=x||xx<=sea.btm.xx) && (sea.top.y!=y||sea.top.yy<=yy) && (sea.btm.y!=y||yy<=sea.btm.yy)){//on est sur cette mer
					if(((!lvl)||lvl!='s')&&sea.islands){
						for(var iid=0,cc=sea.islands.length;iid<cc;iid++){
							if(sea.islands[iid]!=null){
								var island=omap.islands[sea.islands[iid]];
								if(island!=null && island.top.x<=x && x<=island.btm.x && island.top.y<=y && y<= island.btm.y
								&& (island.top.x!=x||island.top.xx<=xx) && (island.btm.x!=x||xx<=island.btm.xx) && (island.top.y!=y||island.top.yy<=yy) && (island.btm.y!=y||yy<=island.btm.yy)){//on est sur cette île
									if(((!lvl)||lvl!='i')&&zoom>=20&&island.cases){
										for(var i=0,ccc=island.cases.length;i<ccc;i++){
											for(var j=0,cccc=island.cases[i].length;j<cccc;j++){
												var coord=cg.addCoords({'x':island.top.x,'y':island.top.y,'xx':island.top.xx,'yy':island.top.yy},{'x':0,'y':0,'xx':0,'yy':0,'xxx':j,'yyy':i})
												if(coord.x==x&&coord.y==y&&coord.xx==xx&&coord.yy==yy&&coord.xxx==xxx&&coord.yyy==yyy){//on est sur cette case
													return {'l':'c','s':sea,'i':island,'c':island.cases[i][j],'cplus':{'x':j,'y':i}};
												}
											}
										}
									}
									return {'l':'i','s':sea,'i':island};
								}
							}
						}
					}
					return {'l':'s','s':sea};
				}	
			}
		}
		return {'l':'o'};
	}
	
	get.rpx2elem=function(lvl,x,y){
		var coor=cg.rpx2coord(x,y);
		return get.coord2elem(lvl,coor.x,coor.y,coor.xx,coor.yy,coor.xxx,coor.yyy);
	}
	
	get.islandSize=function(a){
		return {'x':18*a.btm.x+6*a.btm.xx - (18*a.top.x+6*a.top.xx) + 6,'y':18*a.btm.y+6*a.btm.yy - (18*a.top.y+6*a.top.yy) + 6}}
		
	get.part=function(p){
	
		if((!parts[p]) || (typeof(parts[p])).toLowerCase() != 'object'){
			return parts[p]={'pos':{'type':'i','id':0},'islands':[],'seas':[]};
		}else{
			return parts[p];
		}
	
	}
	
	get.curPart=function(){
		if(!logged){
			return {'pos':{'type':'i','id':0},'islands':[],'seas':[]};
		}else{
			return get.part(opt.curpart);
		}
	}

	omap.move = new function(){
		this.moving = false;
		this.moved=false;
		this.block = false;
		this.startpos={};
		this.start=function(e){
			if(!omap.move.moving){
				omap.move.startpos.x=omap.opos.x-e.clientX;
				omap.move.startpos.y=omap.opos.y-e.clientY;
				omap.move.moving=true;
			}
		}
		this.move=function(e){
			if(omap.move.moving){
				omap.details.move=omap.edit.drawing?false:true;
				omap.opos.x=omap.move.startpos.x+e.clientX;
				omap.opos.y=omap.move.startpos.y+e.clientY;
				omap.move.moved=true;
				draw.redraw();
			}
		}
		this.stop=function(e){
			if(omap.move.moving&&(e.type=='mouseup'||(e.type=='mouseout'&&(!isChildOf(e.relatedTarget,document.getElementById('details')))&&(!isChildOf(e.relatedTarget,document.getElementById('map')))))){
				omap.move.moving=false;
				if(omap.move.move){get.datas();}
				omap.move.moved=false;
			}
		}
	}
	omap.details = new function(){
		this.offsetX=22;
		this.offsetY=22;
		this.move=true;
		this.lock=false;
		this.edit=false;
		this.editOn=false;
		this.editState=false;
		this.wide=false;
		this.toggleBlock=function(){
			if(omap.details.lock){
				omap.details.lock=false;
				$('#blockdetails img')[0].src='./img/other/detailslock.png';
			}else{
				omap.details.lock=true;
				$('#blockdetails img')[0].src='./img/other/detailsunlock.png';
			}
		}
		this.toggleWidth=function(){
			if(omap.details.wide){
				omap.details.wide=false;
				$('#detailswidth img')[0].src='./img/other/detailswidthon.png';
				$('#details').removeClass('wide');
			}else{
				omap.details.wide=true;
				$('#detailswidth img')[0].src='./img/other/detailswidthoff.png';
				$('#details').addClass('wide');
			}
		}
		this.toggleMove=function(e){
			if(!omap.details.lock){
				if(!omap.edit.drawing){
					if(omap.move.moved){
						omap.details.move=true;
						$('.detailsopt,#detailsacts').css('display','none');
					}else{
						if(omap.details.move){
							omap.details.move=false;
							$('.detailsopt').css('display','table-cell');
							$('#detailsacts').css('display','table');
							if(opt['edit']){omap.details.edit=true;};
							var px=cg.rpx2px(e.clientX,e.clientY);
							omap.details.editOn=get.rpx2elem(false,e.clientX,e.clientY);
						}else{
							omap.details.move=true;
							$('.detailsopt,#detailsacts').css('display','none');
						}
					}
				}

			omap.details.show(e);
			}
		}
		this.toggleEdit=function(e){
			if(!omap.details.lock){
				if(omap.details.editOn){
					switch(omap.details.editOn.l){
						case'c':case'i':
							if(!omap.details.editOn.i.st){
								omap.iu.setElemState(2);
							}else{
								omap.iu.setElemState((omap.details.editOn.i.st%4)+1);
							}
						break;
						case's':
							if(!omap.details.editOn.s.st){
								omap.iu.setElemState(2);
							}else{
								omap.iu.setElemState((omap.details.editOn.s.st%4)+1);
							}
						break;
						default://lvl : o
						if(opt['edit']){
							omap.edit.editing='o';omap.edit.drawing=true;omap.details.updateEdit('cancel');
						}
							$('.detailsopt').css('display','table-cell');
							$('#detailsacts').css('display','table');
							omap.details.move=false;
							omap.details.show(e);
					}
				}
				/*$('.detailsopt').css('display','table-cell');
				$('#detailsacts').css('display','table');
				omap.details.move=false;
				omap.details.show(e);*/
			}
		}
		this.updateInfoPos=function(e){
			var left=e.clientX+omap.details.offsetX;
			var top=e.clientY+omap.details.offsetY;	
			if(left+$('#details').width()>window.innerWidth-15){if(e.clientX-$('#details').width()>5){left=e.clientX-$('#details').width()- 15;}}
			if(top+$('#details').height()>window.innerHeight-15){if(e.clientY-$('#details').height()>5){top=e.clientY-$('#details').height()- 15;}}
			if(top<0){top=0};if(left<0){left=0};
			$('#details').css('left',left+'px');
			$('#details').css('top',top+'px');
		}
		this.show=function(e){
			if(omap.details.move){
				var seacoord=cg.rpx2coord(e.clientX,e.clientY);
				var elem=get.coord2elem(false,seacoord.x,seacoord.y,seacoord.xx,seacoord.yy,seacoord.xxx,seacoord.yyy);
				$('#details').css('display','block');
				if(!omap.details.lock){omap.details.updateInfoPos(e);};
				switch(elem.l){
					case 'o':
						$('#detailssea').html('Mare Incognita');
						$('#detailssea').css('display','block')
						$('#detailsisland').css('display','none');
						$('#detailscase').css('display','none');
						$('#detailscontent').css('display','none');
					break;
					case 's':
						$('#detailssea').html(elem.s.name);
						$('#detailscontent').html(elem.s.global);
						$('#detailssea').css('display','block')
						$('#detailsisland').css('display','none');
						$('#detailscase').css('display','none');
						$('#detailscontent').css('display','block');
					break;
					case 'c':
						if(elem.c.s){
							var datas={'title':elem.c.s,'c':JSON.stringify(elem.c)},disps=true,dispc=true;
							switch(elem.c.s){
								case 'nsp':datas.title='Inconnu';dispc=false;break;
								case 'albc':datas.title='Alambique';dispc=false;break;
								case 'autel':datas.title='Autel';
									if(elem.c.c!='nsp'){
										var name;
										switch(parseInt(elem.c.c)){
											case 1:name='Ares';break;
											case 2:name='Déméter';break;
											case 3:name='Atlas';break;
											case 4:name='Poséidon';break;
											case 5:name='Hermès';break;
										}
										datas.c='Ici on vénére '+name;
									}else{
										datas.c='Inconnu';
									}
								break;
								case 'bibli':datas.title='Bibliothèque';dispc=false;break;
								case 'btl':datas.title='Combat';
									if(elem.c.c!='nsp'){
										var a=elem.c.c,ol='';								
										for(var i=0,c=a.length;i<c;i++){
											var name=(a[i]=='nsp')?'Inconnu':res.mob[parseInt(a[i])].n;ol+='<li>'+name+'</li>';}
										datas.c='<ol>'+ol+'</ol>';
									}else{
										datas.c='Inconnu';
									}
								break;
								case 'chst':datas.title='Coffre';
									if(elem.c.c!='nsp'){
										switch(elem.c.c.c1){
											case'map':
												datas.c='Carte ou Boussole';
											break;
											case'gld':
												datas.c=elem.c.c.c2+' pièce'+(elem.c.c.c2>1?'s':'')+' d\'or';
											break;
											case'item':
												var i=res.item[elem.c.c.c2];								
												datas.c=i.n;
											break;
											default:									
										}
									}else{
										datas.c='Inconnu';
									}
								break;
								case 'empt':datas.title='Neutre';dispc=false;break;
								case 'food':datas.title='Nourriture';dispc=false;break;
								case 'gld':datas.title='Or';if(elem.c.c!='nsp'){datas.c=elem.c.c+' pièce'+(elem.c.c>1?'s':'')+' d\'or';}else{datas.c='Inconnu'}break;
								case 'homa':case 'homb':case 'homc':case 'homd':
									switch(elem.c.c.c1){
										case 'pnj':
											datas.title='PNJ : '+elem.c.c.c2;break;
										case 'hero':
											datas.title='Héros : '+res.hero[parseInt(elem.c.c.c2)].n;break;
										default:datas.title='PNJ';
									}
									dispc=false;
								break;
								case 'inn':datas.title='Auberge';
									if(elem.c.c=='nsp'){
										datas.c='Prix inconnu';
									}else{
										datas.c='Regénérer ses runes coûte '+elem.c.c+' pièce'+(elem.c.c>1?'s':'')+' d\'or dans cette auberge.';
									}
								break;
								case 'port':datas.title='Port';dispc=false;break;
								case 'rum':datas.title='Rumeurs';if(elem.c.c!='nsp'){datas.c=elem.c.c}else{datas.c='Inconnu'};break;
								case 'rns':datas.title='Ruines';
									datas.c='';
									if(elem.c.c.c2!='nsp'){
										datas.c+='<p>'+elem.c.c.c2+' pièce'+(elem.c.c.c2>1?'s':'')+' d\'or';
									}else{
										datas.c+='Gains inconnus<br />';
									}
									if(elem.c.c.c1!='nsp'){
										var a=elem.c.c.c1,ol='';								
										for(var i=0,c=a.length;i<c;i++){
											var name=(a[i]=='nsp')?'Inconnu':res.mob[parseInt(a[i])].n;ol+='<li>'+name+'</li>';}
										datas.c+='<ol>'+ol+'</ol>';
									}else{
										datas.c+='<br />Monstres inconnus';
									}
								break;
								case 'tbl':datas.title='Table d\'orientation';dispc=false;break;
								case 'tpl':datas.title='Temple';dispc=false;break;
								case 'vdra':case'vdrb':datas.title='Vendeur';
									if(elem.c.c.c1!='nsp'){
										datas.c='Prix de la nourriture : '+elem.c.c.c1;
									}else{
										datas.c='Inconnu';
									}
								break;
								default:datas.title=elem.i.name;disps=dispc=false;
							}
							$('#detailssea').html(elem.s.name);
							$('#detailsisland').html(elem.i.name);
							$('#detailscase').html(datas.title);
							$('#detailscontent').html(datas.c);
							if(opt['smallinf']){$('#detailssea').css('display','none')}else{$('#detailssea').css('display','block')};
							$('#detailsisland').css('display','block');
							if(disps){
							
							$('#detailscase').css('display','block');
								if(opt['smallinf']){$('#detailsisland').css('display','none')}else{$('#detailsisland').css('display','block')};
							}else{
								$('#detailscase').css('display','none');
							};
							if(dispc){$('#detailscontent').css('display','block');}else{$('#detailscontent').css('display','none');};
							break;
						}
					case 'i':
						$('#detailssea').html(elem.s.name);
						$('#detailsisland').html(elem.i.name);
						$('#detailscontent').html(elem.i.global);
						if(opt['smallinf']){$('#detailssea').css('display','none')}else{$('#detailssea').css('display','block')};
						$('#detailsisland').css('display','block');
						$('#detailscase').css('display','none');
						$('#detailscontent').css('display','block');
					break;
				}
				$('#coordx').html('X : <strong>'+seacoord.x+'</strong>.'+seacoord.xx+'.'+seacoord.xxx);
				$('#coordy').html('Y : <strong>'+seacoord.y+'</strong>.'+seacoord.yy+'.'+seacoord.yyy);
			}
			if(omap.details.edit&&(!omap.details.move)){
				elem=omap.details.editOn;
				if(!omap.edit.editing){
					switch (elem.l){
						case 'o':
							omap.details.updateEdit('do',elem);
						break;
						case 's':
							omap.details.updateEdit('ds',elem);
						break;
						case 'i':
							omap.details.updateEdit('di',elem);
						break;
						case 'c':
							omap.details.updateEdit('dc',elem);
						break;
					}
				}				
				$('#detailsedit').css('display','block');
			}else if(!omap.details.lock){
				$('#detailsedit').css('display','none');
			}

		}
		this.updateEdit=function(v,elem){
			if(v!=omap.details.editState||v=='do'||v=='ds'||v=='di'||v=='dc'){
				$('#detailsedit').empty();
				omap.details.editState=v;
				var submit;
				if(v=='as'||v=='ai'||v=='ac'||v=='se'||v=='ie'||v=='ce'){
					var btn=document.createElement('li');
					btn.innerHTML='Annuler';
					$(btn).click(omap.edit.clear);
					$('#detailsedit').append(btn);
				}
				switch(v){
					case'cancel':
						var btn=document.createElement('li');
						btn.innerHTML='Annuler';
						$(btn).click(omap.edit.clear);
						$('#detailsedit').append(btn);
					break;
					case'do':
						var btn=document.createElement('li');
						btn.innerHTML='Ajouter une mer';
						$(btn).click(omap.edit.enableSea);
						$('#detailsedit').append(btn);
					break;
					case'ds':
						var btn=document.createElement('li');
						btn.innerHTML='Editer la mer';
						$(btn).click(function(e){omap.edit.enableSeaEdit(e,elem.s)});
						$('#detailsedit').append(btn);
						var btn=document.createElement('li');
						btn.innerHTML='Ajouter une île';
						$(btn).click(function(e){omap.edit.enableIsland(e,elem.s)});
						$('#detailsedit').append(btn);
						var btn=document.createElement('li');
						btn.innerHTML='Supprimer la mer';
						$(btn).click(function(e){omap.edit.removeSea(e,elem.s)});
						$('#detailsedit').append(btn);
					break;
					case'di':
						var btn=document.createElement('li');
						btn.innerHTML='Editer l\'île';
						$(btn).click(function(e){omap.edit.enableIslandEdit(e,elem.i)});
						$('#detailsedit').append(btn);
						var btn=document.createElement('li');
						btn.innerHTML='Ajouter une case';
						$(btn).click(function(e){omap.edit.enableCase(e,elem.s,elem.i)});
						$('#detailsedit').append(btn);
						var btn=document.createElement('li');
						btn.innerHTML='Supprimer l\'île';
						$(btn).click(function(e){omap.edit.removeIsland(e,elem.i)});
						$('#detailsedit').append(btn);
					break;
					case 'dc':
						var btn=document.createElement('li');
						btn.innerHTML='Editer l\'île';
						$(btn).click(function(e){omap.edit.enableIslandEdit(e,elem.i)});
						$('#detailsedit').append(btn);
						var btn=document.createElement('li');
						btn.innerHTML='Ajouter une case';
						$(btn).click(function(e){omap.edit.enableCase(e,elem.s,elem.i)});
						$('#detailsedit').append(btn);
						var btn=document.createElement('li');
						btn.innerHTML='Supprimer l\'île';
						$(btn).click(function(e){omap.edit.removeIsland(e,elem.i)});
						$('#detailsedit').append(btn);
						if(elem.c!=0&&elem.c!="0"){
							var btn=document.createElement('li');
							btn.innerHTML='Editer la case';
							$(btn).click(function(e){omap.edit.enableCaseEdit(e,elem.s,elem.i,elem.c,elem.cplus)});
							$('#detailsedit').append(btn);
							var btn=document.createElement('li');
							btn.innerHTML='Supprimer la case';
							$(btn).click(function(e){omap.edit.removeCase(e,elem.i,elem.cplus)});
							$('#detailsedit').append(btn);
						}
					break;
					case'as'://addSea
						var form=document.createElement('li');
						form.innerHTML='<input id="editseaname" /><br /><textarea id="editseaglobal"></textarea>';
						$('#detailsedit').append(form);
						submit='o';
					break;
					case'ai':
						var form=document.createElement('li');
						form.innerHTML='<input id="editislandname" /><br /><textarea id="editislandglobal"></textarea>';
						$('#detailsedit').append(form);
						submit='s';
					break;
					case'ac':
						var li=document.createElement('li');
						omap.edit.islandMapping(omap.islands[omap.edit.cur.i.id]);
						var elem=get.coord2elem(false,omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy,omap.edit.cur.coor1.xxx,omap.edit.cur.coor1.yyy);
						var s=get.islandSize(omap.islands[omap.edit.cur.i.id]);
	
						var pathlist='';
						if(elem.cplus.y>0){
							var ispath='';
							if(parseInt(omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y-1][elem.cplus.x].pb)>0){ispath='checked="checked"';};
							pathlist+='<label class="quart"><input '+ispath+' type="checkbox" id="patht"/>Haut</label>';
						}
						if(elem.cplus.x>0){
							var ispath='';
							if(parseInt(omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x-1].pr)>0){ispath='checked="checked"';};
							pathlist+='<label class="quart"><input '+ispath+' type="checkbox" id="pathl"/>Gauche</label>';
						}
						if(elem.cplus.x<s.x-1){
							var ispath='';
							if(parseInt(elem.c.pr)>0){ispath='checked="checked"';};
							pathlist+='<label class="quart"><input '+ispath+' type="checkbox" id="pathr"/>Droite</label>';
						}

						if(elem.cplus.y<s.y-1){
							var ispath='';
							if(parseInt(elem.c.pb)>0){ispath='checked="checked"';};
							pathlist+='<label class="quart"><input '+ispath+' type="checkbox" id="pathb"/>Bas</label>';
						}

						li.innerHTML='<form>'+pathlist+'</form>';
						$('#detailsedit').append(li);
						li=document.createElement('li');
						li.innerHTML='<form><select name="cs">\
							<option value="nsp">Inconnu</option>\
							<option value="t">Terrain</option>\
							<option value="empt">Neutre</option>\
							<option value="bibli">Bibliothèque</option>\
							<option value="tpl">Temple</option>\
							<option value="tbl">Table d\'Orientation</option>\
							<option value="ftn">Fontaine de Jouvence</option>\
							<option value="albc">Alambique</option>\
							<option value="food">Four</option>\
							<option value="port">Port</option>\
							<option value="btl">Combat</option>\
							<option value="chst">Coffre</option>\
							<option value="hom">Maison</option>\
							<option value="inn">Auberge</option>\
							<option value="autel">Autel</option>\
							<option value="vdr">Marchand</option>\
							<option value="gld">Or</option>\
							<option value="rum">Rumeurs</option>\
							<option value="rns">Ruines</option>\
						</select></form>';
						$('#detailsedit').append(li);
						$('select[name=cs]').change(omap.details.caseEdit);
						li=document.createElement('li');
						li.id='casecontent';
						$(li).css('display','none');
						$('#detailsedit').append(li);
						submit='i';
					break;
					case 'se'://edit
						var form=document.createElement('li');
						form.innerHTML='<input id="editseaname" value="'+omap.edit.cur.s.name+'"/><textarea id="editseaglobal">'+omap.edit.cur.s.global+'</textarea>';
						$('#detailsedit').append(form);
						submit='se';
					break;
					case 'ie':
						var form=document.createElement('li');
						form.innerHTML='<input id="editislandname" value="'+omap.edit.cur.i.name+'"/><textarea id="editislandglobal">'+omap.edit.cur.i.global+'</textarea>';
						$('#detailsedit').append(form);
						submit='ie';
					break;
					case 'ce':
						var li=document.createElement('li');
						omap.edit.islandMapping(omap.islands[omap.edit.cur.i.id]);
						var elem={'s':omap.edit.cur.s,'i':omap.edit.cur.i,'c':omap.edit.cur.c,'cplus':omap.edit.cur.cplus};
						var s=get.islandSize(omap.islands[omap.edit.cur.i.id]);
	
						var pathlist='';
						if(elem.cplus.y>0){
							var ispath='';
							if(parseInt(omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y-1][elem.cplus.x].pb)>0){ispath='checked="checked"';};
							pathlist+='<label class="quart"><input '+ispath+' type="checkbox" id="patht"/>Haut</label>';
						}
						if(elem.cplus.x>0){
							var ispath='';
							if(parseInt(omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x-1].pr)>0){ispath='checked="checked"';};
							pathlist+='<label class="quart"><input '+ispath+' type="checkbox" id="pathl"/>Gauche</label>';
						}
						if(elem.cplus.x<s.x-1){
							var ispath='';
							if(parseInt(elem.c.pr)>0){ispath='checked="checked"';};
							pathlist+='<label class="quart"><input '+ispath+' type="checkbox" id="pathr"/>Droite</label>';
						}
						if(elem.cplus.y<s.y-1){
							var ispath='';
							if(parseInt(elem.c.pb)>0){ispath='checked="checked"';};
							pathlist+='<label class="quart"><input '+ispath+' type="checkbox" id="pathb"/>Bas</label>';
						}
						li.innerHTML='<form>'+pathlist+'</form>';
						$('#detailsedit').append(li);
						li=document.createElement('li');
						li.innerHTML='<form><select name="cs">\
							<option value="nsp">Inconnu</option>\
							<option value="t">Terrain</option>\
							<option value="empt">Neutre</option>\
							<option value="bibli">Bibliothèque</option>\
							<option value="tpl">Temple</option>\
							<option value="tbl">Table d\'Orientation</option>\
							<option value="ftn">Fontaine de Jouvence</option>\
							<option value="albc">Alambique</option>\
							<option value="food">Four</option>\
							<option value="port">Port</option>\
							<option value="btl">Combat</option>\
							<option value="chst">Coffre</option>\
							<option value="hom">Maison</option>\
							<option value="inn">Auberge</option>\
							<option value="autel">Autel</option>\
							<option value="vdr">Marchand</option>\
							<option value="gld">Or</option>\
							<option value="rum">Rumeurs</option>\
							<option value="rns">Ruines</option>\
						</select></form>';
						$('#detailsedit').append(li);
						$('select[name=cs]').change(omap.details.caseEdit);
						li=document.createElement('li');
						li.id='casecontent';
						$(li).css('display','none');
						$('#detailsedit').append(li);
						submit='ce';
					break;
				}
				if(submit){
					var btn=document.createElement('li');
					btn.innerHTML='Envoyer';
					$(btn).click(function(){omap.edit.send(submit);});
					$('#detailsedit').append(btn);
				}
			}
		}//minautaure reine guêpe rototo sorcier crépuscule 300 PO
		this.chstEdit=function(e){
			var v = e.target.value;
			$('#chstc').empty();
			switch(v){
				case'gld':
					$('#chstc').append('<label><input name="ngld" value="Quantité d\'or"/></label>');
				break;
				case'item':
					var itemlist='';for(var i=1, c=res.item.length;i<c;i++){itemlist+='<option value="'+i+'">'+res.item[i].n+'</option>';}
					$('#chstc').append('<select name="item">'+itemlist+'</select>');
				break;
			}
		}
		this.homEdit=function(e){
			var v = e.target.value;
			$('#homc').empty();
			switch(v){
				case'pnj':
					$('#homc').append('<label><input name="pnjname" value="Nom du PNJ"/></label>');
				break;
				case'hero':
					var herolist='';for(var i=1, c=res.hero.length;i<c;i++){herolist+='<option value="'+i+'">'+res.hero[i].n+'</option>';}
					$('#homc').append('<select name="hero">'+herolist+'</select>');
				break;
			}
		}
		this.vdrEdit=function(e){
			var v = e.target.value;
			$('#homc').empty();
			switch(v){
				case'pnj':
					$('#homc').append('<label><input name="pnjname" value="Nom du PNJ"/></label>');
				break;
				case'hero':
					var herolist='';for(var i=1, c=res.hero.length;i<c;i++){herolist+='<option value="'+i+'">'+res.hero[i].n+'</option>';}
					$('#homc').append('<select name="hero">'+herolist+'</select>');
				break;
			}
		}
		this.caseEdit=function(e){
			var v = e.target.value;
			if(v!=omap.details.editCaseState){
				$('#casecontent').empty();
				omap.details.editCaseState=v;
				var show=true;
				switch(v){
					case'autel':
						var form=document.createElement('form');
						form.innerHTML='<select name="auteltype" id="auteltype"><option value="nsp">Inconnu</option><option value="1">Ares</option><option value="2">Déméter</option><option value="3">Atlas</option><option value="4">Poséidon</option><option value="5">Hermès</option></select>';
						$('#casecontent').append(form);
					break;
					case'btl':
						var form=document.createElement('form'),select=document.createElement('select');
						var moblist='';for(var i=1, c=res.mob.length;i<c;i++){moblist+='<option value="'+i+'">'+res.mob[i].n+'</option>';}
						form.id='btlmobs';
						select.innerHTML='<option value="0">Aucun</option><option value="nsp">Inconnu</option>'+moblist;
						$(form).append('<input type="checkbox" legend="Inconnu" id="nsp" /><img alt="?" src="./img/other/nsp.png"/>');
						$(form).append(select);
						$('#casecontent').append(form);
						$(select).change(function(e){if(parseInt($(e.target).val())==0&&$('#btlmobs select[value=0]').length>1){$(e.target).remove();}else if(parseInt($(e.target).val())!=0&&$('#btlmobs select[value=0]').length<1){var slct=$(e.target).clone(true);$('#btlmobs').append(slct)}});
					break;
					case'chst':
						var form=document.createElement('form');
						form.innerHTML='<select name="chsttype" id="chsttype"><option value="nsp">Inconnu</option><option value="map">Carte ou boussole</option><option value="gld">Or</option><option value="item">Objet</option></select>';
						$(form).append('<div id="chstc"></div>');
						$('#casecontent').append(form);
						$('#chsttype').change(omap.details.chstEdit);
					break;
					case'gld':
						var form=document.createElement('form');
						form.innerHTML='<label><input type="checkbox" legend="Inconnu" id="nsp" /><img alt="?" src="./img/other/nsp.png"/><input name="ngld" value="Quantité d\'or"/></label>';
						$('#casecontent').append(form);
					break;
					case'hom':
						var form=document.createElement('form');
						form.innerHTML='<select name="homstyle" id="homstyle"><option value="nsp">Inconnu</option><option value="a">Pierre</option><option value="b">Arbre</option><option value="c">Hutte</option><option value="d">Carrée</option></select>\
						<br /><select name="homtype" id="homtype"><option value="nsp">PNJ - nom inconnu</option><option value="pnj">PNJ</option><option value="hero">Héros</option></select>';
						$(form).append('<div id="homc"></div>');
						$('#casecontent').append(form);
						$('#homtype').change(omap.details.homEdit);
					break;
					case'inn':
						var form=document.createElement('form');
						form.innerHTML='<label><input type="checkbox" legend="Inconnu" id="nsp" /><img alt="?" src="./img/other/nsp.png"/><input name="innp" value="Prix de l\'auberge"/></label>';
						$('#casecontent').append(form);
					break;
					case'port':
						var form=document.createElement('form');
						form.innerHTML='<label class="quart"><input type="radio" name="pos" value="t"/>Haut</label>\
						<label class="quart"><input type="radio" name="pos" value="l"/>Gauche</label>\
						<label class="quart"><input type="radio" name="pos" value="r"/>Droite</label>\
						<label class="quart"><input type="radio" name="pos" value="b"/>Bas</label>';
						$('#casecontent').append(form);
					break;
					case'rns':
						var form=document.createElement('form'),select=document.createElement('select');
						form.innerHTML='<label><input type="checkbox" legend="Inconnu" id="nsp2" /><img alt="?" src="./img/other/nsp.png"/><input name="ngld" value="Quantité d\'or"/></label>';
						var moblist='';for(var i=1, c=res.mob.length;i<c;i++){moblist+='<option value="'+i+'">'+res.mob[i].n+'</option>';}
						form.id='btlmobs';
						select.innerHTML='<option value="0">Aucun</option><option value="nsp">Inconnu</option>'+moblist;
						$(form).append('<br /><input type="checkbox" legend="Inconnu" id="nsp" /><img alt="?" src="./img/other/nsp.png"/>');
						$(form).append(select);
						$('#casecontent').append(form);
						$(select).change(function(e){if(parseInt($(e.target).val())==0&&$('#btlmobs select[value=0]').length>1){$(e.target).remove();}else if(parseInt($(e.target).val())!=0&&$('#btlmobs select[value=0]').length<1){var slct=$(e.target).clone(true);$('#btlmobs').append(slct)}});

					break;
					case'rum':
						var form=document.createElement('form');
						form.innerHTML='<label><input type="checkbox" legend="Inconnu" id="nsp" /><img alt="?" src="./img/other/nsp.png"/><textarea name="rumc"/>On raconte que...</textarea></label>';
						$('#casecontent').append(form);
					break;
					case'vdr':
						var form=document.createElement('form');
						form.innerHTML='<select name="vdrstyle" id="vdrstyle"><option value="nsp">Inconnu</option><option value="a">Tente</option><option value="b">Stand</option></select><br />\
						<label><input type="checkbox" legend="Inconnu" id="nsp" /><img alt="?" src="./img/other/nsp.png"/><input name="foodp" value="Prix de la nourriture"/></label>';
						$('#casecontent').append(form);
					break;
					default:
						show=false;
				}
				if(show){$('#casecontent').css('display','block');}else{$('#casecontent').css('display','none');};
			}
		}
		this.hide=function(e){
			if(omap.details.move&&(!omap.details.lock)){$('#details').css('display','none');}
		}
	}
	omap.zoom=new function(){
	
		this.zoomer=function(z,c){
			if(zoom!=z){
				setTimeout(function(){
					omap.opos.x= z>zoom ? parseInt( (omap.opos.x-(c.x/2)+(ctxt.size.w/4)) * z/zoom ) : parseInt(omap.opos.x*z/zoom);
					omap.opos.y= z>zoom ? parseInt( (omap.opos.y-(c.y/2)+(ctxt.size.h/4)) * z/zoom ) : parseInt(omap.opos.y*z/zoom);
					zoom=z;
					get.datas();
					draw.redraw();
				},100);
			}
		};
		
		this.delta2val=function(d){
			var nz=zoom;
			if(d>0){
				if(zoom<1){nz=1;
				}else if(zoom<2){nz=2;
				}else if(zoom<5){nz=5;
				}else if(zoom<10){nz=10;
				}else if(zoom<20){nz=20;
				}else if(zoom<50){nz=50;
				}else{nz=zoom;}
			}else if(d<0){
				if(zoom>20){nz=20;
				}else if(zoom>10){nz=10;
				}else if(zoom>5){nz=5;
				}else if(zoom>2){nz=2;
				}else if(zoom>1){nz=1;
				}else{nz=0.5;}
			}
			return nz;
		};
		
		this.wheelEvent=function(e,d){
			omap.zoom.zoomer(omap.zoom.delta2val(d),{'x':e.clientX,'y':e.clientY});
		};
		
		this.simpleZoomer=function(z){
			if(zoom!=z){
				setTimeout(function(){
					omap.opos.x= parseInt(omap.opos.x*z/zoom);
					omap.opos.y= parseInt(omap.opos.y*z/zoom);
					zoom=z;
					get.datas();
					draw.redraw();
				},100);
			}
		};
	
	}
	
	omap.edit=new function(){
	
		this.editing=false;
		this.cur={};
		this.right=false;
		this.drawn=false;
		this.drawing=false;
		this.attachedform=false;
		this.clear=function(){
			omap.edit.editing=false;
			omap.edit.cur={};
			omap.edit.right=false;
			omap.edit.drawn=false;
			omap.edit.drawing=false;
			omap.edit.attachedform=false;
			omap.details.editCaseState=false;
			draw.redraw();
			omap.details.show();
		}
		this.enableSea=function(e){
			omap.edit.editing='o';omap.edit.drawing=true;omap.details.updateEdit('cancel');omap.details.show(e);}
		this.enableIsland=function(e,s){
			omap.edit.editing='s';
			omap.edit.drawing=true;
			omap.edit.cur.s=s;
			omap.details.updateEdit('cancel');
			omap.details.show(e);
		}
		this.enableCase=function(e,s,i){
			omap.edit.editing='i';
			omap.edit.drawing=true;
			omap.edit.cur.s=s;
			omap.edit.cur.i=i;
			omap.details.updateEdit('cancel');
			omap.details.show(e);
		}
		this.enableSeaEdit=function(e,s){
			omap.edit.editing='se';
			omap.edit.drawing=false;
			omap.edit.cur.s=s;
			omap.details.updateEdit('se');
			omap.details.show(e);
		}
		this.enableIslandEdit=function(e,i){
			omap.edit.editing='ie';
			omap.edit.drawing=false;
			omap.edit.cur.i=i;
			omap.details.updateEdit('ie');
			omap.details.show(e);
		}
		this.enableCaseEdit=function(e,s,i,c,cp){
			omap.edit.editing='ce';
			omap.edit.drawing=true;
			omap.edit.cur.s=s;
			omap.edit.cur.i=i;
			omap.edit.cur.c=c;
			omap.edit.cur.cplus=cp;
			omap.details.updateEdit('ce');
			omap.details.show(e);
		}
		this.removeSea=function(e,s){
			if(confirm('Attention, cette action est irréversible !\nEtes vous sur de vouloir continuer ?')){
				$.ajax({
					type: "POST",
					url:'./tools/datas.php',
					data:{'setomap':'rms','id':s.id},
					complete:function(r){
						var rep;
						try{
							rep=JSON.parse(r.responseText);
						}catch(er){
							alert('Erreur lors de la lecture du JSON');
							return;
						}
						if(rep.error==0){
							omap.seas[rep.id]=null;
							draw.redraw();
						}else{
							alert('Une erreur est survenue');
						}
					
					}
				});
			}
		}
		this.removeIsland=function(e,i){
			if(confirm('Attention, cette action est irréversible !\nEtes vous sur de vouloir continuer ?')){
				$.ajax({
					type: "POST",
					url:'./tools/datas.php',
					data:{'setomap':'rmi','id':i.id},
					complete:function(r){
						var rep;
						try{
							rep=JSON.parse(r.responseText);
						}catch(er){
							alert('Erreur lors de la lecture du JSON');
							return;
						}
						if(rep.error==0){
							omap.islands[rep.id]=null;
							draw.redraw();
						}else{
							alert('Une erreur est survenue');
						}
					}
				});
			}
		}
		this.removeCase=function(e,i,cplus){
			if(confirm('Attention, cette action est irréversible !\nEtes vous sur de vouloir continuer ?')){
				$.ajax({
					type: "POST",
					url:'./tools/datas.php',
					data:{'setomap':'rmc','id':i.id,'x':cplus.x,'y':cplus.y},
					complete:function(r){
						var rep;
						try{
							rep=JSON.parse(r.responseText);
						}catch(er){
							alert('Erreur lors de la lecture du JSON');
							return;
						}
						if(rep.error==0){
							omap.islands[rep.id].cases[rep.y][rep.x]=0;
							if(rep.y>0 && type(omap.islands[rep.id].cases[rep.y-1][rep.x])=='object'){
								omap.islands[rep.id].cases[rep.y-1][rep.x].pb=0;
							}
							if(rep.y>0 && type(omap.islands[rep.id].cases[rep.y][rep.x-1])=='object'){
								omap.islands[rep.id].cases[rep.y][rep.x-1].pr=0;
							}
							draw.redraw();
						}else{
							alert('Une erreur est survenue');
						}
					}
				});
			}
		}
		this.islandMapping=function(a){
			if((!a.cases)||a.cases==''){
				var j = 18*a.btm.y+6*a.btm.yy - 18*a.top.y+6*a.top.yy + 6;
				var jj = 18*a.btm.x+6*a.btm.xx;
				jj-=18*a.top.x+6*a.top.xx;
				jj+=6;
				var cases=[];
				for(var i=0;i<j;i++){
				cases.push([]);
					for(var ii=0;ii<jj;ii++){
						cases[i].push(0);
					}
				}
				a.cases=clone(cases);
			}
		};
		this.draw=function(){
		
			if(omap.edit.editing){
				switch(omap.edit.editing){
					case'o':
			
						if(omap.edit.cur.coor1){
							if(omap.edit.cur.c1placed==false){
								var coora=cg.coord2rpx(omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy);
								var elem=get.rpx2elem(false,coora.x,coora.y), c;
								if(elem.l=='o'){c=[0,255,0,0.3];omap.edit.right=true;}else{c=[255,0,0,0.3];omap.edit.right=false;}
								ctxt.innerBoxShadow(coora.x,coora.y,6*zoom-1,6*zoom-1,c,1.5*zoom);
							}else if(!omap.edit.cur.c2placed){
								var coora=cg.coord2rpx(omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy);
								var coorb=cg.coord2rpx(omap.edit.cur.coor2.x,omap.edit.cur.coor2.y,omap.edit.cur.coor2.xx,omap.edit.cur.coor2.yy);
								var coor1={'x':coora.x<=coorb.x?coora.x:coorb.x,'y':coora.y<=coorb.y?coora.y:coorb.y};
								var coor2={'x':coora.x<=coorb.x?coorb.x:coora.x,'y':coora.y<=coorb.y?coorb.y:coora.y};
								var coort=cg.rpx2coord(coor1.x,coor1.y);
								var coort2=cg.rpx2coord(coor1.x,coor1.y);
								var coor2=cg.rpx2coord(coor2.x,coor2.y);
								var coor2=cg.addCoords(coor2,{'x':0,'y':0,'xx':1,'yy':1});
								var c=false;
								while((coort.x!=coor2.x||coort.xx!=coor2.xx)&&(!c)){
									while((coort.y!=coor2.y||coort.yy!=coor2.yy)&&(!c)){
										var real=cg.coord2rpx(coort.x,coort.y,coort.xx,coort.yy);
										var elem=get.rpx2elem(false,real.x,real.y);
										if(elem.l!='o'){c=[255,0,0,0.3];omap.edit.right=false;}
										coort=cg.addCoords(coort,{'x':0,'y':0,'xx':0,'yy':1});
									}
									coort=cg.addCoords({'x':coort.x,'y':coort2.y,'xx':coort.xx,'yy':coort2.yy},{'x':0,'y':0,'xx':1,'yy':0});
								}
								if(!c){c=[0,255,0,0.3];omap.edit.right=true;}
								var coor2=cg.coord2rpx(coor2.x,coor2.y,coor2.xx,coor2.yy);
								ctxt.innerBoxShadow(coor1.x,coor1.y,coor2.x-coor1.x-1,coor2.y-coor1.y-1,c,1.5*zoom);
							}else{
								var coora=cg.coord2rpx(omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy);
								var coorb=cg.coord2rpx(omap.edit.cur.coor2.x,omap.edit.cur.coor2.y,omap.edit.cur.coor2.xx,omap.edit.cur.coor2.yy);
								var coor1={'x':coora.x<=coorb.x?coora.x:coorb.x,'y':coora.y<=coorb.y?coora.y:coorb.y};
								var coor2={'x':coora.x<=coorb.x?coorb.x:coora.x,'y':coora.y<=coorb.y?coorb.y:coora.y};
								var coor2=cg.rpx2coord(coor2.x,coor2.y);
								var coor2=cg.ncoord2rpx(coor2.x,coor2.y,coor2.xx,coor2.yy);
								var c=[0,255,0,0.3];
								ctxt.innerBoxShadow(coor1.x,coor1.y,coor2.x-coor1.x-1,coor2.y-coor1.y-1,c,1.5*zoom);
							
							}
						}
								
								
					break;
					
					case's':
					
						if(omap.edit.cur.coor1){
							if(omap.edit.cur.c1placed==false){
								var coora=cg.coord2rpx(omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy);
								var elem=get.rpx2elem(false,coora.x,coora.y);
								if(elem.l=='s'&&elem.s.id==omap.edit.cur.s.id){
									ctxt.fillStyle='rgba(0,255,0,0.3)';omap.edit.right=true;
								}else{
									ctxt.fillStyle='rgba(255,0,0,0.3)';omap.edit.right=false;
								}
								ctxt.beginPath();
								ctxt.arc(coora.x+(6/4)*zoom,coora.y+(6/4)*zoom,(6/8)*zoom,Math.PI*1.5,Math.PI,true);
								ctxt.lineTo(coora.x+(6/8)*zoom,coora.y+3*(6/4)*zoom);
								ctxt.arc(coora.x+(6/4)*zoom,coora.y+3*(6/4)*zoom,(6/8)*zoom,Math.PI,Math.PI*0.5,true);
								ctxt.lineTo(coora.x+3*(6/4)*zoom,coora.y+7*(6/8)*zoom);
								ctxt.arc(coora.x+3*(6/4)*zoom,coora.y+3*(6/4)*zoom,(6/8)*zoom,Math.PI*0.5,0,true);
								ctxt.lineTo(coora.x+7*(6/8)*zoom,coora.y+(6/4)*zoom);
								ctxt.arc(coora.x+3*(6/4)*zoom,coora.y+(6/4)*zoom,(6/8)*zoom,0,Math.PI*1.5,true);
								ctxt.closePath();
								ctxt.fill();
							}else if(!omap.edit.cur.c2placed){
								var coora=cg.coord2rpx(omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy);
								var coorb=cg.coord2rpx(omap.edit.cur.coor2.x,omap.edit.cur.coor2.y,omap.edit.cur.coor2.xx,omap.edit.cur.coor2.yy);
								var coor1={'x':coora.x<=coorb.x?coora.x:coorb.x,'y':coora.y<=coorb.y?coora.y:coorb.y};
								var coor2={'x':coora.x<=coorb.x?coorb.x:coora.x,'y':coora.y<=coorb.y?coorb.y:coora.y};
								var coort=cg.rpx2coord(coor1.x,coor1.y);
								var coort2=cg.rpx2coord(coor1.x,coor1.y);
								var coor2=cg.rpx2coord(coor2.x,coor2.y);
								var coor2=cg.addCoords(coor2,{'x':0,'y':0,'xx':1,'yy':1});
								var c=false;
								while((coort.x!=coor2.x||coort.xx!=coor2.xx)&&(!c)){
									while((coort.y!=coor2.y||coort.yy!=coor2.yy)&&(!c)){
										//alert(JSON.stringify(coor2));
										var real=cg.coord2rpx(coort.x,coort.y,coort.xx,coort.yy);
										var elem=get.rpx2elem(false,real.x,real.y);
										if(elem.l!='s'||elem.s.id!=omap.edit.cur.s.id){c=[255,0,0,0.3];omap.edit.right=false;}
										coort=cg.addCoords(coort,{'x':0,'y':0,'xx':0,'yy':1});
									}
									coort=cg.addCoords({'x':coort.x,'y':coort2.y,'xx':coort.xx,'yy':coort2.yy},{'x':0,'y':0,'xx':1,'yy':0});
								}
								if(!c){c=[0,255,0,0.3];omap.edit.right=true;}
								var coor2=cg.coord2rpx(coor2.x,coor2.y,coor2.xx,coor2.yy);
								ctxt.fillStyle='rgba('+c[0]+','+c[1]+','+c[2]+','+c[3]+')';
								ctxt.beginPath();
								ctxt.arc(coor1.x+(6/4)*zoom,coor1.y+(6/4)*zoom,(6/8)*zoom,Math.PI*1.5,Math.PI,true);
								ctxt.lineTo(coor1.x+(6/8)*zoom,coor2.y-(6/4)*zoom);
								ctxt.arc(coor1.x+(6/4)*zoom,coor2.y-(6/4)*zoom,(6/8)*zoom,Math.PI,Math.PI*0.5,true);
								ctxt.lineTo(coor2.x-(6/4)*zoom,coor2.y-(6/8)*zoom);
								ctxt.arc(coor2.x-(6/4)*zoom,coor2.y-(6/4)*zoom,(6/8)*zoom,Math.PI*0.5,0,true);
								ctxt.lineTo(coor2.x-(6/8)*zoom,coor1.y+(6/4)*zoom);
								ctxt.arc(coor2.x-(6/4)*zoom,coor1.y+(6/4)*zoom,(6/8)*zoom,0,Math.PI*1.5,true);
								ctxt.closePath();
								ctxt.fill();
							}else{
								var coora=cg.coord2rpx(omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy);
								var coorb=cg.coord2rpx(omap.edit.cur.coor2.x,omap.edit.cur.coor2.y,omap.edit.cur.coor2.xx,omap.edit.cur.coor2.yy);
								var coor1={'x':coora.x<=coorb.x?coora.x:coorb.x,'y':coora.y<=coorb.y?coora.y:coorb.y};
								var coor2={'x':coora.x<=coorb.x?coorb.x:coora.x,'y':coora.y<=coorb.y?coorb.y:coora.y};
								var coor2=cg.rpx2coord(coor2.x,coor2.y);
								var coor2=cg.ncoord2rpx(coor2.x,coor2.y,coor2.xx,coor2.yy);
								ctxt.fillStyle='rgba(0,255,0,0.3)';
								ctxt.beginPath();
								ctxt.arc(coor1.x+(6/4)*zoom,coor1.y+(6/4)*zoom,(6/8)*zoom,Math.PI*1.5,Math.PI,true);
								ctxt.lineTo(coor1.x+(6/8)*zoom,coor2.y-(6/4)*zoom);
								ctxt.arc(coor1.x+(6/4)*zoom,coor2.y-(6/4)*zoom,(6/8)*zoom,Math.PI,Math.PI*0.5,true);
								ctxt.lineTo(coor2.x-(6/4)*zoom,coor2.y-(6/8)*zoom);
								ctxt.arc(coor2.x-(6/4)*zoom,coor2.y-(6/4)*zoom,(6/8)*zoom,Math.PI*0.5,0,true);
								ctxt.lineTo(coor2.x-(6/8)*zoom,coor1.y+(6/4)*zoom);
								ctxt.arc(coor2.x-(6/4)*zoom,coor1.y+(6/4)*zoom,(6/8)*zoom,0,Math.PI*1.5,true);
								ctxt.closePath();
								ctxt.fill();
							}
						}
					break;
					
					case'i':
						if(omap.edit.cur.coor1){
							if(omap.edit.cur.c1placed==false){
								var coora=cg.coord2rpx(omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy,omap.edit.cur.coor1.xxx,omap.edit.cur.coor1.yyy);
								var elem=get.rpx2elem(false,coora.x,coora.y);
								if((elem.l=='i'||(elem.l=='c'&&(elem.c==0&&elem.c=="0")))&&elem.i.id==omap.edit.cur.i.id){
									ctxt.fillStyle='rgba(0,255,0,0.3)';omap.edit.right=true;
								}else{
									ctxt.fillStyle='rgba(255,0,0,0.3)';omap.edit.right=false;
								}
								ctxt.beginPath();
								ctxt.arc(coora.x+2*zoom/6,coora.y+2*zoom/6,zoom/6,Math.PI*1.5,Math.PI,true);
								ctxt.lineTo(coora.x+zoom/6,coora.y+4*zoom/6);
								ctxt.arc(coora.x+2*zoom/6,coora.y+4*zoom/6,zoom/6,Math.PI,Math.PI*0.5,true);
								ctxt.lineTo(coora.x+4*zoom/6,coora.y+5*zoom/6);
								ctxt.arc(coora.x+4*zoom/6,coora.y+4*zoom/6,zoom/6,Math.PI*0.5,0,true);
								ctxt.lineTo(coora.x+5*zoom/6,coora.y+2*zoom/6);
								ctxt.arc(coora.x+4*zoom/6,coora.y+2*zoom/6,zoom/6,0,Math.PI*1.5,true);
								ctxt.closePath();
								ctxt.fill();
							}else{
								var coora=cg.coord2rpx(omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy,omap.edit.cur.coor1.xxx,omap.edit.cur.coor1.yyy);
								ctxt.fillStyle='rgba(0,255,0,0.3)';
								ctxt.beginPath();
								ctxt.arc(coora.x+2*zoom/6,coora.y+2*zoom/6,zoom/6,Math.PI*1.5,Math.PI,true);
								ctxt.lineTo(coora.x+zoom/6,coora.y+4*zoom/6);
								ctxt.arc(coora.x+2*zoom/6,coora.y+4*zoom/6,zoom/6,Math.PI,Math.PI*0.5,true);
								ctxt.lineTo(coora.x+4*zoom/6,coora.y+5*zoom/6);
								ctxt.arc(coora.x+4*zoom/6,coora.y+4*zoom/6,zoom/6,Math.PI*0.5,0,true);
								ctxt.lineTo(coora.x+5*zoom/6,coora.y+2*zoom/6);
								ctxt.arc(coora.x+4*zoom/6,coora.y+2*zoom/6,zoom/6,0,Math.PI*1.5,true);
								ctxt.closePath();
								ctxt.fill();
							}
						}
					break;
				}
			}
		}
		this.move=function(e){
			var red=draw.redraw;
			if(omap.edit.editing){
				switch(omap.edit.editing){
					case'o':
						if(!omap.edit.cur.c2placed){
							if(omap.edit.cur.c1placed){
								omap.edit.cur.coor2=cg.rpx2coord(e.clientX,e.clientY);
							}else{
								omap.edit.cur.coor1=cg.rpx2coord(e.clientX,e.clientY)
								omap.edit.cur.c1placed=false;
							}
							red();
						}
					break;
					case's'://on ajoute une île
						if(!omap.edit.cur.c2placed){
							if(omap.edit.cur.c1placed){
								omap.edit.cur.coor2=cg.rpx2coord(e.clientX,e.clientY);
							}else{
								omap.edit.cur.coor1=cg.rpx2coord(e.clientX,e.clientY)
								omap.edit.cur.c1placed=false;
							}
							red();
						}
					break;
					case'i'://on ajoute une case
						if(omap.edit.cur.c1placed){
							omap.edit.cur.coor2=cg.rpx2coord(e.clientX,e.clientY);
						}else{
							omap.edit.cur.coor1=cg.rpx2coord(e.clientX,e.clientY)
							omap.edit.cur.c1placed=false;
						}
						red();
					break;
				}
			}
		}
		this.click=function(e){
			var red=draw.redraw;
			if(omap.edit.editing&&(!omap.move.moved)){
				switch(omap.edit.editing){
					case'o':
						if(omap.edit.right){
							if(!omap.edit.cur.coor2){
								omap.edit.cur.c1placed=true;
								omap.edit.cur.coor2=cg.rpx2coord(e.clientX,e.clientY);
							}else{
								omap.edit.cur.c2placed=true;
								omap.edit.drawn=true;
								omap.details.updateEdit('as');
							}
						}red();
					break;
					case's':
						if(omap.edit.right){
							if(!omap.edit.cur.coor2){
								omap.edit.cur.c1placed=true;
								omap.edit.cur.coor2=cg.rpx2coord(e.clientX,e.clientY);
							}else{
								omap.edit.cur.c2placed=true;
								omap.edit.drawn=true;
								omap.details.updateEdit('ai');
							}
						}red();
					break;
					case'i':
						if(omap.edit.right){
							omap.edit.cur.c1placed=true;
							omap.edit.drawn=true;
							omap.details.updateEdit('ac');
						}red();
					break;					
				}
			}
		}
		this.send=function(v){
			switch(v){
				case'o':
					var coora=cg.coord2rpx(omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy);
					var coorb=cg.coord2rpx(omap.edit.cur.coor2.x,omap.edit.cur.coor2.y,omap.edit.cur.coor2.xx,omap.edit.cur.coor2.yy);
					var coor1={'x':coora.x<=coorb.x?coora.x:coorb.x,'y':coora.y<=coorb.y?coora.y:coorb.y};
					var coor2={'x':coora.x<=coorb.x?coorb.x:coora.x,'y':coora.y<=coorb.y?coorb.y:coora.y};
					var coor1=cg.rpx2coord(coor1.x,coor1.y);
					var coor2=cg.rpx2coord(coor2.x,coor2.y);
					var name=$('#editseaname').val();
					var global=$('#editseaglobal').val();
					$.ajax({
						type: "POST",
						url:'./tools/datas.php',
						data: 'setomap=ns&x1='+coor1.x+'&y1='+coor1.y+'&xx1='+coor1.xx+'&yy1='+coor1.yy+'&x2='+coor2.x+'&y2='+coor2.y+'&xx2='+coor2.xx+'&yy2='+coor2.yy+'&name='+name+'&gbl='+global,
						complete:function(r){
							var rep = jQuery.parseJSON(r.responseText);
							if(parseInt(rep.error)!=0){
								alert('Erreur');
							}else{
								var sea=rep.sea;
								for(var c in sea.top){
									sea.top[c]=parseInt(sea.top[c]);
								}for(var c in sea.btm){
									sea.btm[c]=parseInt(sea.btm[c]);
								}
								sea.id=parseInt(rep.id);
								omap.seas[sea.id]=sea;
							}
							omap.edit.clear();
							draw.redraw();
						}
					});
				break;
				case's':
					var coora=cg.coord2rpx(omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy);
					var coorb=cg.coord2rpx(omap.edit.cur.coor2.x,omap.edit.cur.coor2.y,omap.edit.cur.coor2.xx,omap.edit.cur.coor2.yy);
					var coor1={'x':coora.x<=coorb.x?coora.x:coorb.x,'y':coora.y<=coorb.y?coora.y:coorb.y};
					var coor2={'x':coora.x<=coorb.x?coorb.x:coora.x,'y':coora.y<=coorb.y?coorb.y:coora.y};
					var coor1=cg.rpx2coord(coor1.x,coor1.y);
					var coor2=cg.rpx2coord(coor2.x,coor2.y);
					var name=$('#editislandname').val();
					var global=$('#editislandglobal').val();
					var id=omap.edit.cur.s.id;
					$.ajax({
						type: "POST",
						url:'./tools/datas.php',
						data: 'setomap=ni&id='+id+'&x1='+coor1.x+'&y1='+coor1.y+'&xx1='+coor1.xx+'&yy1='+coor1.yy+'&x2='+coor2.x+'&y2='+coor2.y+'&xx2='+coor2.xx+'&yy2='+coor2.yy+'&name='+name+'&gbl='+global,
						complete:function(r){
							var rep = jQuery.parseJSON(r.responseText);
							if(parseInt(rep.error)!=0){
								alert('Erreur');
							}else{
								var id=parseInt(rep.id);
								var island=rep.island;
								island.sea=parseInt(rep.island.sea);
								for(var c in island.top){
									island.top[c]=parseInt(island.top[c]);
								}for(var c in island.btm){
									island.btm[c]=parseInt(island.btm[c]);
								}
								island.id=id;
								omap.seas[island.sea].islands[id]=island;
							}
							omap.edit.clear();
							draw.redraw();
						}
					});
				break;
				case'i':
					var rep=$('select[name=cs]');
					var paths=[$('#pathr').is(':checked'),$('#pathb').is(':checked'),$('#patht').is(':checked'),$('#pathl').is(':checked')];
					if(rep.length==1){
						var island = omap.edit.cur.i;
						omap.edit.islandMapping(omap.islands[omap.edit.cur.i.id]);
						var elem=get.coord2elem(false,omap.edit.cur.coor1.x,omap.edit.cur.coor1.y,omap.edit.cur.coor1.xx,omap.edit.cur.coor1.yy,omap.edit.cur.coor1.xxx,omap.edit.cur.coor1.yyy);

						var lieu={};
						lieu.c='';
						switch(rep.val()){
							case 'albc':  case 'autel': case 'bibli': case 'tbl': case 'tpl': case 'food': case 'ftn':
							case 'inn': case 'btl': case 'chst': case 'empt': case 'gld': case 'port':
							case 'rum': case 'rns': case 't': case 'nsp':
								lieu.s=rep.val();
							break;
							case 'hom':
								var homstyle=$('select[name=homstyle]');
								if(homstyle.length==1){
									switch(homstyle.val()){
										case'a':lieu.s='homa';break;
										case'b':lieu.s='homb';break;
										case'c':lieu.s='homc';break;
										case'd':lieu.s='homd';break;
										default:lieu.s='homa';lieu.c={'nsp':'on'};break;
									}
								}else{
									lieu.c={'nsp':'on'};
								}
							break;
							case 'vdr':
								var vdrstyle=$('select[name=vdrstyle]');
								if(vdrstyle.length==1){
									switch(vdrstyle.val()){
										case'a':lieu.s='vdra';break;
										case'b':lieu.s='vdrb';break;
										case'nsp':lieu.s='vdrb';lieu.c={'nsp':'on'};break;
										default:lieu.s='vdrb';lieu.c={'nsp':'on'};break;
									}
								}else{
									lieu.c={'nsp':'on'};
								}
							break;
							default:alert('Erreur au niveau du type de case.');return;
						}
						switch(rep.val()){
							case 'autel':
								var type=$('select[name=auteltype]');
								if(type.length==1){
									lieu.c=type.val();
								}else{alert('Erreur');return false;}
							break;
							case 'btl':
								if($('#nsp:checked').length==0&&$('#btlmobs select[value!=0]').length>=1){
									var btlmobs=$('#btlmobs select[value!=0]')
									if(btlmobs.size()>=1){
										lieu.c=[];
										btlmobs.each(function(i,e){
											if(parseInt($(e).val())!=0){lieu.c.push($(e).val()=='nsp'?'nsp':parseInt($(e).val()))};
										});
									}else{alert('Erreur au niveau du comptage des monstres.');return false;}
								}else{
									lieu.c='nsp';
								}
							break;
							case 'chst':
								var chsttype=$('select[name=chsttype]');
								if(chsttype.val()!='nsp'&&chsttype.length==1){
									switch(chsttype.val()){
										case'map':
											lieu.c={'c1':'map'};
										break;
										case'gld':
											var ngld=$('input[name=ngld]');
											if(ngld.length==1){
												lieu.c={'c1':'gld','c2':ngld.val()};
											}else{alert('Vous devez renseigner une quantité d\'or.');return false;}
										break;
										case'item':
											var item=$('select[name=item]');
											if(item.size()==1){
												lieu.c={'c1':'item','c2':parseInt(item.val())};
											}else{alert('Vous devez renseigner l\'objet contenu dans le coffre.');return false;}
										break;
										default:
										alert('Bien essayé mais votre tentative de sabotage a été repérée. ;-)');return false;
									}
								}else{
									lieu.c='nsp';
								}
							break;
							case 'gld':
								var ngld=$('input[name=ngld]');
								if($('#nsp:checked').length==0&&ngld.length==1){
									lieu.c=ngld.val();
								}else{
									lieu.c='nsp';
								}
							break;
							case 'inn':
								var innp=$('input[name=innp]');
								if($('#nsp:checked').length==0&&innp.length==1){
									lieu.c=innp.val();
								}else{
									lieu.c='nsp';
								}
							break;
							case 'hom':
								if(!lieu.c){lieu.c={}};
								var homtype=$('select[name=homtype]');
								if(homtype.length==1){
									switch(homtype.val()){
										case'nsp':
											lieu.c.c1='nsp';
										break;
										case'pnj':
											var pnj=$('input[name=pnjname]');
											if(pnj.length==1){
												lieu.c.c1='pnj';lieu.c.c2=pnj.val();
											}else{alert('Vous devez renseigner un nom.');return false;}
										break;
										case'hero':
											var hero=$('select[name=hero]');
											if(hero.size()==1){
												lieu.c.c1='hero';lieu.c.c2=parseInt(hero.val());
											}else{alert('Vous devez renseigner le nom du héro.');return false;}
										break;
										default:
										alert('Bien essayé mais votre tentative de sabotage a été repérée. ;-)');return false;
									}
								}else{alert('Erreur');return false;}
							break;
							case 'port':
								var pos=$('input[type=radio][name=pos]:checked');
								if(pos.length==1){
									lieu.c=pos.val();
								}else{alert('Vous devez positionner le port.');return false;}
							break;
							case 'rum':
								var homtype=$('textarea[name=rumc]');
								if($('#nsp:checked').length==0&&homtype.length==1){
									lieu.c=homtype.val();
								}else{
									lieu.c='nsp';
								}
							break;
							case 'vdr':
								if(!lieu.c){lieu.c={}};
								var foodp=$('input[name=foodp]');
								if($('#nsp:checked').length==0&&foodp.length==1){
									lieu.c.c1=foodp.val();
								}else{
									lieu.c.c1='nsp';
								}
							break;
							case 'rns':
								lieu.c={};
								if($('#nsp:checked').length==0&&$('#btlmobs select[value!=0]').length>=1){
									var btlmobs=$('#btlmobs select')
									if(btlmobs.size()>=1){
										lieu.c.c1=[];
										btlmobs.each(function(i,e){
											if(parseInt($(e).val())!=0){lieu.c.c1.push($(e).val()=='nsp'?'nsp':parseInt($(e).val()))};
										});
									}else{alert('Erreur au niveau du comptage des monstres.');return false;}
								}else{
									lieu.c.c1='nsp';
								}
								if($('#nsp2:checked').length==0){
									lieu.c.c2=parseInt($('input[name=ngld]').val());
								}else{
									lieu.c.c2='nsp';
								}
							break;
						}
						if(paths[0]){//droite
							var dr=omap.islands[elem.i.id].cases[elem.cplus.y][elem.cplus.x+1];
							if((!dr)||(dr==0)){
								dr={'s':'nsp'};
							}
							lieu.pr=1;
						}
						if(paths[1]){//bas
							var bt=omap.islands[elem.i.id].cases[elem.cplus.y+1][elem.cplus.x];
							if((!bt)||(bt==0)){
								bt={'s':'nsp'};
							}
							lieu.pb=1;
						}
						if(paths[2]){//haut
							if(!omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y-1][elem.cplus.x]||omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y-1][elem.cplus.x]==0){omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y-1][elem.cplus.x]={'s':'nsp','pb':1};
							}else{omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y-1][elem.cplus.x].pb=1;}
						}
						if(paths[3]){//gauche
							if(!omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x-1]||omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x-1]==0){omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x-1]={'s':'nsp','pr':1};
							}else{omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x-1].pr=1;}
						}

						omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x]=lieu

						var id=elem.i.id;
						$.ajax({
							type: "POST",
							url:'./tools/datas.php',
							data: 'setomap=nc&id='+id+'&x='+elem.cplus.x+'&y='+elem.cplus.y+'&s='+lieu.s+'&c='+JSON.stringify(lieu.c)+'&p='+JSON.stringify(paths),
							complete:function(rep){omap.edit.clear();}
						});						
					}else{
						alert('Erreur de sélection');
					}
				break;
				case'se':
					var id=omap.edit.cur.s.id;
					var name=$('#editseaname').val();
					var global=$('#editseaglobal').val();
					$.ajax({
						type: "POST",
						url:'./tools/datas.php',
						data: 'setomap=se&id='+id+'&name='+name+'&gbl='+global,
						complete:function(r){
							var rep;
							try{
								rep=JSON.parse(r.responseText);
							}catch(er){
								alert('Erreur lors de la lecture du JSON');
								return;
							}
							if(rep.error==0){
								omap.seas[rep.sea.id].name=rep.sea.name;
								omap.seas[rep.sea.id].global=rep.sea.global;
								draw.redraw();
							}else{
								alert('Une erreur est survenue');
							}
							omap.edit.clear();
						}
					});
				break;
				case'ie':
					var id=omap.edit.cur.i.id;
					var name=$('#editislandname').val();
					var global=$('#editislandglobal').val();
					$.ajax({
						type: "POST",
						url:'./tools/datas.php',
						data: 'setomap=ie&id='+id+'&name='+name+'&gbl='+global,
						complete:function(r){
							var rep;
							try{
								rep=JSON.parse(r.responseText);
							}catch(er){
								alert('Erreur lors de la lecture du JSON');
								return;
							}
							if(rep.error==0){
								omap.islands[rep.i.id].name=rep.i.name;
								omap.islands[rep.i.id].global=rep.i.global;
								draw.redraw();
							}else{
								alert('Une erreur est survenue');
							}
							omap.edit.clear();
						}
					});
				break;
				case 'ce':
					var rep=$('select[name=cs]');
					var paths=[$('#pathr').is(':checked'),$('#pathb').is(':checked'),$('#patht').is(':checked'),$('#pathl').is(':checked')];
					if(rep.length==1){
						var island = omap.edit.cur.i;
						omap.edit.islandMapping(omap.islands[omap.edit.cur.i.id]);
						var elem={'s':omap.edit.cur.s,'i':omap.edit.cur.i,'c':omap.edit.cur.c,'cplus':omap.edit.cur.cplus};
						var lieu={};
						lieu.c='';
						switch(rep.val()){
							case 'albc':  case 'autel': case 'bibli': case 'tbl': case 'tpl': case 'food': case 'ftn':
							case 'inn': case 'btl': case 'chst': case 'empt': case 'gld': case 'port':
							case 'rum': case 'rns': case 't': case 'nsp':
								lieu.s=rep.val();
							break;
							case 'hom':
								var homstyle=$('select[name=homstyle]');
								if(homstyle.length==1){
									switch(homstyle.val()){
										case'a':lieu.s='homa';break;
										case'b':lieu.s='homb';break;
										case'c':lieu.s='homc';break;
										case'd':lieu.s='homd';break;
										default:lieu.s='homa';lieu.c={'nsp':'on'};break;
									}
								}else{
									lieu.c={'nsp':'on'};
								}
							break;
							case 'vdr':
								var vdrstyle=$('select[name=vdrstyle]');
								if(vdrstyle.length==1){
									switch(vdrstyle.val()){
										case'a':lieu.s='vdra';break;
										case'b':lieu.s='vdrb';break;
										case'nsp':lieu.s='vdrb';lieu.c={'nsp':'on'};break;
										default:lieu.s='vdrb';lieu.c={'nsp':'on'};break;
									}
								}else{
									lieu.c={'nsp':'on'};
								}
							break;
							default:alert('Erreur au niveau du type de case.');return;
						}
						switch(rep.val()){
							case 'autel':
								var type=$('select[name=auteltype]');
								if(type.length==1){
									lieu.c=type.val();
								}else{alert('Erreur');return false;}
							break;
							case 'btl':
								if($('#nsp:checked').length==0&&$('#btlmobs select[value!=0]').length>=1){
									var btlmobs=$('#btlmobs select[value!=0]')
									if(btlmobs.size()>=1){
										lieu.c=[];
										btlmobs.each(function(i,e){
											if(parseInt($(e).val())!=0){lieu.c.push($(e).val()=='nsp'?'nsp':parseInt($(e).val()))};
										});
									}else{alert('Erreur au niveau du comptage des monstres.');return false;}
								}else{
									lieu.c='nsp';
								}
							break;
							case 'chst':
								var chsttype=$('select[name=chsttype]');
								if(chsttype.val()!='nsp'&&chsttype.length==1){
									switch(chsttype.val()){
										case'map':
											lieu.c={'c1':'map'};
										break;
										case'gld':
											var ngld=$('input[name=ngld]');
											if(ngld.length==1){
												lieu.c={'c1':'gld','c2':ngld.val()};
											}else{alert('Vous devez renseigner une quantité d\'or.');return false;}
										break;
										case'item':
											var item=$('select[name=item]');
											if(item.size()==1){
												lieu.c={'c1':'item','c2':parseInt(item.val())};
											}else{alert('Vous devez renseigner l\'objet contenu dans le coffre.');return false;}
										break;
										default:
										alert('Bien essayé mais votre tentative de sabotage a été repérée. ;-)');return false;
									}
								}else{
									lieu.c='nsp';
								}
							break;
							case 'gld':
								var ngld=$('input[name=ngld]');
								if($('#nsp:checked').length==0&&ngld.length==1){
									lieu.c=ngld.val();
								}else{
									lieu.c='nsp';
								}
							break;
							case 'inn':
								var innp=$('input[name=innp]');
								if($('#nsp:checked').length==0&&innp.length==1){
									lieu.c=innp.val();
								}else{
									lieu.c='nsp';
								}
							break;
							case 'hom':
								if(!lieu.c){lieu.c={}};
								var homtype=$('select[name=homtype]');
								if(homtype.length==1){
									switch(homtype.val()){
										case'nsp':
											lieu.c.c1='nsp';
										break;
										case'pnj':
											var pnj=$('input[name=pnjname]');
											if(pnj.length==1){
												lieu.c.c1='pnj';lieu.c.c2=pnj.val();
											}else{alert('Vous devez renseigner un nom.');return false;}
										break;
										case'hero':
											var hero=$('select[name=hero]');
											if(hero.size()==1){
												lieu.c.c1='hero';lieu.c.c2=parseInt(hero.val());
											}else{alert('Vous devez renseigner le nom du héro.');return false;}
										break;
										default:
										alert('Bien essayé mais votre tentative de sabotage a été repérée. ;-)');return false;
									}
								}else{alert('Erreur');return false;}
							break;
							case 'port':
								var pos=$('input[type=radio][name=pos]:checked');
								if(pos.length==1){
									lieu.c=pos.val();
								}else{alert('Vous devez positionner le port.');return false;}
							break;
							case 'rum':
								var homtype=$('textarea[name=rumc]');
								if($('#nsp:checked').length==0&&homtype.length==1){
									lieu.c=homtype.val();
								}else{
									lieu.c='nsp';
								}
							break;
							case 'vdr':
								if(!lieu.c){lieu.c={}};
								var foodp=$('input[name=foodp]');
								if($('#nsp:checked').length==0&&foodp.length==1){
									lieu.c.c1=foodp.val();
								}else{
									lieu.c.c1='nsp';
								}
							break;
							case 'rns':
								lieu.c={};
								if($('#nsp:checked').length==0&&$('#btlmobs select[value!=0]').length>=1){
									var btlmobs=$('#btlmobs select')
									if(btlmobs.size()>=1){
										lieu.c.c1=[];
										btlmobs.each(function(i,e){
											if(parseInt($(e).val())!=0){lieu.c.c1.push($(e).val()=='nsp'?'nsp':parseInt($(e).val()))};
										});
									}else{alert('Erreur au niveau du comptage des monstres.');return false;}
								}else{
									lieu.c.c1='nsp';
								}
								if($('#nsp2:checked').length==0){
									lieu.c.c2=parseInt($('input[name=ngld]').val());
								}else{
									lieu.c.c2='nsp';
								}
							break;
						}
						if(paths[0]){//droite
							var dr=omap.islands[elem.i.id].cases[elem.cplus.y][elem.cplus.x+1];
							if((!dr)||(dr==0)){
								dr={'s':'nsp'};
							}
							lieu.pr=1;
						}
						if(paths[1]){//bas
							var bt=omap.islands[elem.i.id].cases[elem.cplus.y+1][elem.cplus.x];
							if((!bt)||(bt==0)){
								bt={'s':'nsp'};
							}
							lieu.pb=1;
						}
						if(paths[2]){//haut
							if(!omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y-1][elem.cplus.x]||omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y-1][elem.cplus.x]==0){omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y-1][elem.cplus.x]={'s':'nsp','pb':1};
							}else{omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y-1][elem.cplus.x].pb=1;}
						}
						if(paths[3]){//gauche
							if(!omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x-1]||omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x-1]==0){omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x-1]={'s':'nsp','pr':1};
							}else{omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x-1].pr=1;}
						}

						omap.islands[omap.edit.cur.i.id].cases[elem.cplus.y][elem.cplus.x]=lieu

						var id=elem.i.id;
						$.ajax({
							type: "POST",
							url:'./tools/datas.php',
							data: 'setomap=ce&id='+id+'&x='+elem.cplus.x+'&y='+elem.cplus.y+'&s='+lieu.s+'&c='+JSON.stringify(lieu.c)+'&p='+JSON.stringify(paths),
							complete:function(rep){omap.edit.clear();}
						});						
					}else{
						alert('Erreur de sélection');
					}
				break;
			}
		}
	}
	
	omap.iu=new function(){
		this.rate=function(b){
			if(omap.details.editOn){
				switch(omap.details.editOn.l){
					case 's':
						$.ajax({
							type: "POST",
							url:'./tools/datas.php',
							data:{'rateomap':'s','rate':b,'id':omap.details.editOn.s.id},
							complete:function(r){/*alert(r.responseText)*/}
						});
					break;
					case 'i':
						$.ajax({
							type: "POST",
							url:'./tools/datas.php',
							data:{'rateomap':'i','rate':b,'id':omap.details.editOn.i.id},
							complete:function(r){/*alert(r.responseText)*/}
						});
					break;
					case 'c':
						$.ajax({
							type: "POST",
							url:'./tools/datas.php',
							data:{'rateomap':'c','rate':b,'id':omap.details.editOn.i.id,'x':omap.details.editOn.cplus.x,'y':omap.details.editOn.cplus.y},
							complete:function(r){/*alert(r.responseText)*/}
						});
					break;
				}
			}
		}
		this.setCurPosition=function(){
			if(omap.details.editOn){
				switch(omap.details.editOn.l){
					case 'c': case 'i':
						$.ajax({
							type: "POST",
							url:'./tools/datas.php',
							data:{'setuser':'pos','elem':'i','id':omap.details.editOn.i.id},
							complete:function(r){rep=JSON.parse(r.responseText);get.curPart().pos={'type':rep.pos.type,'id':rep.pos.id};draw.redraw()}
						});
					break;
					case 's':
						$.ajax({
							type: "POST",
							url:'./tools/datas.php',
							data:{'setuser':'pos','elem':'s','id':omap.details.editOn.s.id},
							complete:function(r){rep=JSON.parse(r.responseText);get.curPart().pos={'type':rep.pos.type,'id':rep.pos.id};draw.redraw()}
						});
					break;
					default:
						alert('Impossible de définir sa position à cet endroit.');
				}
			}
		}
		this.switchCurPart=function(p){
			switch(parseInt(p)){
				case 2:p=2;break;
				case 3:p=3;break;
				case 4:p=4;break;
				default:p=1;
			}
			$.ajax({
				type: "POST",
				url:'./tools/datas.php',
				data:{'setuser':'curpart','part':p},
				complete:function(r){omap.iu.applyPartSwitching(r)}
			});
		}
		
		this.applyPartSwitching=function(r){
		
		
		}
		
		this.setElemState=function(st){
			if(omap.details.editOn){
				switch(omap.details.editOn.l){
					case 'c': case 'i':
						$.ajax({
							type: "POST",
							url:'./tools/datas.php',
							data:{'setuser':'completion','elem':'i','id':omap.details.editOn.i.id,'st':st},
							complete:function(r){omap.iu.adjustElemState(r)}
						});
					break;
					case 's':
						$.ajax({
							type: "POST",
							url:'./tools/datas.php',
							data:{'setuser':'completion','elem':'s','id':omap.details.editOn.s.id,'st':st},
							complete:function(r){omap.iu.adjustElemState(r)}
						});
					break;
					default:
						alert('Impossible de définir l\'état d\'avancement vis-à-vis de cet élément.');
				}
			}
		}
		this.adjustElemState=function(rep){
			var r = JSON.parse(rep.responseText);
			switch(r.ct){
				case 's':
					omap.seas[r.s.id].st=r.s.st;
					draw.redraw();
				break;
				case 'i':
					omap.islands[r.i.id].st=r.i.st;
					draw.redraw();
				break;
			}
		}
	}
	

	
	var zoom=1;
	ctxt.resize.full();
	cg.mcoord2opos(0,0);
	draw.redraw();
	setCookie('fs','[]',365*24*3600);
	setCookie('fi','[]',365*24*3600);
	setCookie('fc','[]',365*24*3600);
	get.datas();

	var jmap = $('#map')
	
		jmap.mousedown(omap.move.start);
		$('*').mousemove(function(e){omap.move.move(e);omap.edit.move(e);omap.details.show(e)});
		jmap.mouseup(function(e){omap.details.toggleMove(e);omap.edit.click(e)});
		$('*').mouseup(function(e){omap.move.stop(e)});
		$(window).mouseout(function(e){omap.move.stop(e);omap.details.hide(e)});
		jmap.dblclick(function(e){omap.details.toggleEdit(e)});
		jmap.mousewheel(omap.zoom.wheelEvent,true);
		$(window).resize(function() {ctxt.resize.full();draw.redraw();});
		$('#blockdetails').click(function(){omap.details.toggleBlock();});
		$('#detailswidth').click(function(){omap.details.toggleWidth();});
		
		$('#gotoorigin').click(function(){cg.mcoord2opos(0,0);draw.redraw();get.datas();});
		$('#gotomypos').click(function(){
			var c;
			if(c=get.elem2coord('i',get.curPart().pos.id)){
				cg.mcoord2opos(c.x,c.y,c.xx,c.yy);
			}else{
				cg.mcoord2opos(0,0);
			}
			draw.redraw();
			get.datas();
		});
		$('#gotocustom').click(function(){
			var v=[],jq=$('#goto input');
			for(var i=0;i<6;i++){v.push($(jq[i]).val())}
			if(v[0]!=''&&v[3]!=''){
				v[0]=parseInt(v[0]);v[3]=parseInt(v[3]);
				if(v[1]!=''&&v[4]!=''){
					v[1]=(parseInt(v[1]))%3;v[4]=(parseInt(v[4]))%3;
					if(v[2]!=''&&v[5]!=''){
						v[2]=(parseInt(v[2]))%6;v[5]=(parseInt(v[5]))%6;
						cg.mcoord2opos(v[0],v[3],v[1],v[4],v[2],v[5]);
					}else{
						cg.mcoord2opos(v[0],v[3],v[1],v[4]);
					}
				}else{
					cg.mcoord2opos(v[0],v[3]);
				}
			}else{
				cg.mcoord2opos(0,0);
			}
			draw.redraw();
			get.datas();
		});	
		$('#topmenu #zoomslist li').each(function(i,e){
			var zoom=parseFloat($(e).data('zoom'));
			$(e).click(function(){omap.zoom.simpleZoomer(zoom)});
		});
		$('#logform').submit(function(e){
			$.ajax({
				type: "POST",
				url:'./tools/identification.php',
				data:{'loguid':$('#loguid').val(),'logpk':$('#logpk').val(),'rememberlog':($('#rememberlog').is(':checked'))?1:0,'ajaj':1},
				complete:function(r){var rep=JSON.parse(r.responseText);$('#logmenutitle').html(rep.name);$('#logform').replaceWith('Se déconnecter');}
			});
			return false;
		;})
		
	$('#topmenu ul.top li ul.sub').css('top',$('#topmenu').outerHeight()-2+'px');
    $("ul.sub").parent().append("<span class=\"arrow\"></span>");
	$("#topmenu ul.top li span").click(function() {
		$(this).parent().find("ul.sub").slideDown('fast').show();
		$(this).parent().hover(function() {
		}, function(){
			if(!$(this).parent().find("ul.sub input").is(':focus')){
				$(this).parent().find("ul.sub").slideUp('slow');
			}
		});
	});
	
	$('#infgood').click(function(){omap.iu.rate(true)});
	$('#infbad').click(function(){omap.iu.rate(false)});
	$('#imhere').click(function(){omap.iu.setCurPosition()});
	$('#st_u').click(function(){omap.iu.setElemState(1)});
	$('#st_v').click(function(){omap.iu.setElemState(2)});
	$('#st_e').click(function(){omap.iu.setElemState(3)});
	$('#st_c').click(function(){omap.iu.setElemState(4)});
		
}