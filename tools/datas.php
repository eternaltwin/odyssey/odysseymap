<?php include_once("./connectmysqldatas.php"); $cooks=array('fs'=>JSON_decode($_COOKIE['fs']),'fi'=>JSON_decode($_COOKIE['fi']),'fc'=>JSON_decode($_COOKIE['fc']));
session_start();
function get_numeric($val){
  if (is_numeric($val)) {
    return $val + 0;
  }
  return 0;
}

function array_iskey_diff($a,$b){
	$c=$a;
	$d=count($b);
	for ($i = 0; $i < $d; $i++){
		if(array_key_exists($b[$i],$c)){
			unset($c[$b[$i]]);
		}
	}
	return $c;
}


if(isset($_POST['getomaplvl'])){
	switch($_POST['getomaplvl']){
		case 1 :
			if(isset($_POST['x1']) AND isset($_POST['y1']) AND isset($_POST['x2']) AND isset($_POST['y2'])){
				$c=array('x1'=>$_POST['x1'],'y1'=>$_POST['y1'],'x2'=>$_POST['x2'],'y2'=>$_POST['y2']);
				if(is_numeric($c['x1']) AND is_numeric($c['y1']) AND is_numeric($c['x2']) AND is_numeric($c['y2'])){
					$c['x1']=get_numeric($c['x1']);
					$c['y1']=get_numeric($c['y1']);
					$c['x2']=get_numeric($c['x2']);
					$c['y2']=get_numeric($c['y2']);
					if(is_int($c['x1']) AND is_int($c['y1']) AND is_int($c['x2']) AND is_int($c['y2'])){
						$repSQL=$bdd->prepare("SELECT * 
							FROM omap_ocean
							WHERE (:x1<=x1 AND x1<=:x2) OR (:x1<=x2 AND x2<=:x2)
							OR (:y1<=y1 AND y1<=:y2) OR (:y1<=y2 AND y2<=:y2)
							OR (x1<=:x1 AND :x2<=x2) OR (y1<=:y1 AND :y2<=y2)");
						$repSQL->execute(array(':x1'=>$c['x1'],':y1'=>$c['y1'],':x2'=>$c['x2'],':y2'=>$c['y2']));
						while($sea=$repSQL->fetch()){
							$datas['seas'][$sea['id']]['top']['x']=$sea['x1'];
							$datas['seas'][$sea['id']]['top']['y']=$sea['y1'];
							$datas['seas'][$sea['id']]['top']['xx']=$sea['xx1'];
							$datas['seas'][$sea['id']]['top']['yy']=$sea['yy1'];
							$datas['seas'][$sea['id']]['btm']['x']=$sea['x2'];
							$datas['seas'][$sea['id']]['btm']['y']=$sea['y2'];
							$datas['seas'][$sea['id']]['btm']['xx']=$sea['xx2'];
							$datas['seas'][$sea['id']]['btm']['yy']=$sea['yy2'];
							$datas['seas'][$sea['id']]['name']=$sea['name'];
							$datas['seas'][$sea['id']]['global']=nl2br(htmlspecialchars($sea['global']));
						}
						
						$datas['seas']=array_iskey_diff($datas['seas'],$cooks['fs']);
						
						$datas['error']='0';
					}else{
						$datas['error']='202';
						$datas['erdesc']='No integrer datas sent';
					}
				}else{
					$datas['error']='201';
					$datas['erdesc']='No numeric datas sent';
				}
			}else{
				$datas['error']='200';
				$datas['erdesc']='No datas sent';
			}
		break;

		case 2 :
			if(isset($_POST['x1']) AND isset($_POST['y1']) AND isset($_POST['x2']) AND isset($_POST['y2'])){
				$c=array('x1'=>$_POST['x1'],'y1'=>$_POST['y1'],'x2'=>$_POST['x2'],'y2'=>$_POST['y2']);
				if(is_numeric($c['x1']) AND is_numeric($c['y1']) AND is_numeric($c['x2']) AND is_numeric($c['y2'])){
					$c['x1']=get_numeric($c['x1']);
					$c['y1']=get_numeric($c['y1']);
					$c['x2']=get_numeric($c['x2']);
					$c['y2']=get_numeric($c['y2']);
					if(is_int($c['x1']) AND is_int($c['y1']) AND is_int($c['x2']) AND is_int($c['y2'])){
						$repSQL=$bdd->prepare("SELECT * 
							FROM omap_ocean
							WHERE (:x1<=x1 AND x1<=:x2) OR (:x1<=x2 AND x2<=:x2)
							OR (:y1<=y1 AND y1<=:y2) OR (:y1<=y2 AND y2<=:y2)
							OR (x1<=:x1 AND :x2<=x2) OR (y1<=:y1 AND :y2<=y2)");
						$repSQL->execute(array(':x1'=>$c['x1'],':y1'=>$c['y1'],':x2'=>$c['x2'],':y2'=>$c['y2']));
						while($sea=$repSQL->fetch()){
							$sea['id']=get_numeric($sea['id']);
							$datas['seas'][$sea['id']]['top']['x']=$sea['x1'];
							$datas['seas'][$sea['id']]['top']['y']=$sea['y1'];
							$datas['seas'][$sea['id']]['top']['xx']=$sea['xx1'];
							$datas['seas'][$sea['id']]['top']['yy']=$sea['yy1'];
							$datas['seas'][$sea['id']]['btm']['x']=$sea['x2'];
							$datas['seas'][$sea['id']]['btm']['y']=$sea['y2'];
							$datas['seas'][$sea['id']]['btm']['xx']=$sea['xx2'];
							$datas['seas'][$sea['id']]['btm']['yy']=$sea['yy2'];
							$datas['seas'][$sea['id']]['name']=$sea['name'];
							$datas['seas'][$sea['id']]['global']=nl2br(htmlspecialchars($sea['global']));
						}
						
						$datas['seas']=array_iskey_diff($datas['seas'],$cooks['fs']);

						$repSQL=$bdd->prepare("SELECT id,x1,y1,xx1,yy1,x2,y2,xx2,yy2,name,sea,global 
							FROM omap_islands
							WHERE (:x1<=x1 AND x1<=:x2) OR (:x1<=x2 AND x2<=:x2)
							OR (:y1<=y1 AND y1<=:y2) OR (:y1<=y2 AND y2<=:y2)
							OR (x1<=:x1 AND :x2<=x2) OR (y1<=:y1 AND :y2<=y2)");
						$repSQL->execute(array(':x1'=>$c['x1'],':y1'=>$c['y1'],':x2'=>$c['x2'],':y2'=>$c['y2']));
						while($island=$repSQL->fetch()){
							$datas['islands'][$island['id']]['top']['x']=$island['x1'];
							$datas['islands'][$island['id']]['top']['y']=$island['y1'];
							$datas['islands'][$island['id']]['top']['xx']=$island['xx1'];
							$datas['islands'][$island['id']]['top']['yy']=$island['yy1'];
							$datas['islands'][$island['id']]['btm']['x']=$island['x2'];
							$datas['islands'][$island['id']]['btm']['y']=$island['y2'];
							$datas['islands'][$island['id']]['btm']['xx']=$island['xx2'];
							$datas['islands'][$island['id']]['btm']['yy']=$island['yy2'];
							$datas['islands'][$island['id']]['name']=$island['name'];
							$datas['islands'][$island['id']]['sea']=get_numeric($island['sea']);
							$datas['islands'][$island['id']]['global']=nl2br(htmlspecialchars($island['global']));
						}
						
						$datas['islands']=array_iskey_diff($datas['islands'],$cooks['fi']);
						
						$datas['error']='0';
					}else{
						$datas['error']='202';
						$datas['erdesc']='No integrer datas sent';
					}
				}else{
					$datas['error']='201';
					$datas['erdesc']='No numeric datas sent';
				}
			}else{
				$datas['error']='200';
				$datas['erdesc']='No datas sent';
			}
		break;

		case 3 :
			if(isset($_POST['x1']) AND isset($_POST['y1']) AND isset($_POST['x2']) AND isset($_POST['y2'])){
				$c=array('x1'=>$_POST['x1'],'y1'=>$_POST['y1'],'x2'=>$_POST['x2'],'y2'=>$_POST['y2']);
				if(is_numeric($c['x1']) AND is_numeric($c['y1']) AND is_numeric($c['x2']) AND is_numeric($c['y2'])){
					$c['x1']=get_numeric($c['x1']);
					$c['y1']=get_numeric($c['y1']);
					$c['x2']=get_numeric($c['x2']);
					$c['y2']=get_numeric($c['y2']);
					if(is_int($c['x1']) AND is_int($c['y1']) AND is_int($c['x2']) AND is_int($c['y2'])){
						$repSQL=$bdd->prepare("SELECT * 
							FROM omap_ocean
							WHERE (:x1<=x1 AND x1<=:x2) OR (:x1<=x2 AND x2<=:x2)
							OR (:y1<=y1 AND y1<=:y2) OR (:y1<=y2 AND y2<=:y2)
							OR (x1<=:x1 AND :x2<=x2) OR (y1<=:y1 AND :y2<=y2)");
						$repSQL->execute(array(':x1'=>$c['x1'],':y1'=>$c['y1'],':x2'=>$c['x2'],':y2'=>$c['y2']));
						while($sea=$repSQL->fetch()){
							$sea['id']=get_numeric($sea['id']);
							$datas['seas'][$sea['id']]['top']['x']=$sea['x1'];
							$datas['seas'][$sea['id']]['top']['y']=$sea['y1'];
							$datas['seas'][$sea['id']]['top']['xx']=$sea['xx1'];
							$datas['seas'][$sea['id']]['top']['yy']=$sea['yy1'];
							$datas['seas'][$sea['id']]['btm']['x']=$sea['x2'];
							$datas['seas'][$sea['id']]['btm']['y']=$sea['y2'];
							$datas['seas'][$sea['id']]['btm']['xx']=$sea['xx2'];
							$datas['seas'][$sea['id']]['btm']['yy']=$sea['yy2'];
							$datas['seas'][$sea['id']]['name']=$sea['name'];
							$datas['seas'][$sea['id']]['global']=nl2br(htmlspecialchars($sea['global']));
						}
						
						$datas['seas']=array_iskey_diff($datas['seas'],$cooks['fs']);

						$repSQL=$bdd->prepare("SELECT id,x1,y1,xx1,yy1,x2,y2,xx2,yy2,name,sea,global,cases 
							FROM omap_islands
							WHERE (:x1<=x1 AND x1<=:x2) OR (:x1<=x2 AND x2<=:x2)
							OR (:y1<=y1 AND y1<=:y2) OR (:y1<=y2 AND y2<=:y2)
							OR (x1<=:x1 AND :x2<=x2) OR (y1<=:y1 AND :y2<=y2)");
						$repSQL->execute(array(':x1'=>$c['x1'],':y1'=>$c['y1'],':x2'=>$c['x2'],':y2'=>$c['y2']));
						while($island=$repSQL->fetch()){
							$datas['islands'][$island['id']]['top']['x']=$island['x1'];
							$datas['islands'][$island['id']]['top']['y']=$island['y1'];
							$datas['islands'][$island['id']]['top']['xx']=$island['xx1'];
							$datas['islands'][$island['id']]['top']['yy']=$island['yy1'];
							$datas['islands'][$island['id']]['btm']['x']=$island['x2'];
							$datas['islands'][$island['id']]['btm']['y']=$island['y2'];
							$datas['islands'][$island['id']]['btm']['xx']=$island['xx2'];
							$datas['islands'][$island['id']]['btm']['yy']=$island['yy2'];
							$datas['islands'][$island['id']]['name']=$island['name'];
							$datas['islands'][$island['id']]['sea']=get_numeric($island['sea']);
							$datas['islands'][$island['id']]['global']=nl2br(htmlspecialchars($island['global']));
							$datas['islands'][$island['id']]['cases']=$island['cases'];
						}
						
						$datas['islands']=array_iskey_diff($datas['islands'],$cooks['fc']);
						
						$datas['error']='0';
					}else{
						$datas['error']='202';
						$datas['erdesc']='No integrer datas sent';
					}
				}else{
					$datas['error']='201';
					$datas['erdesc']='No numeric datas sent';
				}
			}else{
				$datas['error']='200';
				$datas['erdesc']='No datas sent';
			}
		break;
		default :
		$datas['error']='101';
	}
}elseif(isset($_POST['setomap'])){
	if(isset($_SESSION['id']) AND isset($_SESSION['edit']) AND $_SESSION['edit']==1){
		switch($_POST['setomap']){
			case 'ns' :
				if(isset($_POST['x1']) AND isset($_POST['y1']) AND isset($_POST['x2']) AND isset($_POST['y2']) AND isset($_POST['xx1']) AND isset($_POST['yy1']) AND isset($_POST['xx2']) AND isset($_POST['yy2']) AND isset($_POST['name']) AND isset($_POST['gbl'])){
				$c=array('x1'=>$_POST['x1'],'y1'=>$_POST['y1'],'x2'=>$_POST['x2'],'y2'=>$_POST['y2'],'xx1'=>$_POST['xx1'],'yy1'=>$_POST['yy1'],'xx2'=>$_POST['xx2'],'yy2'=>$_POST['yy2']);
				$c['name']=htmlspecialchars($_POST['name']);
				$c['gbl']=htmlspecialchars($_POST['gbl']);
					if(is_numeric($c['x1']) AND is_numeric($c['y1']) AND is_numeric($c['x2']) AND is_numeric($c['y2']) AND is_numeric($c['xx1']) AND is_numeric($c['yy1']) AND is_numeric($c['xx2']) AND is_numeric($c['yy2'])){
						$c['x1']=get_numeric($c['x1']);
						$c['y1']=get_numeric($c['y1']);
						$c['x2']=get_numeric($c['x2']);
						$c['y2']=get_numeric($c['y2']);
						$c['xx1']=get_numeric($c['xx1']);
						$c['yy1']=get_numeric($c['yy1']);
						$c['xx2']=get_numeric($c['xx2']);
						$c['yy2']=get_numeric($c['yy2']);
						if(is_int($c['x1']) AND is_int($c['y1']) AND is_int($c['x2']) AND is_int($c['y2']) AND is_int($c['xx1']) AND is_int($c['yy1']) AND is_int($c['xx2']) AND is_int($c['yy2'])){
							$reqSQL=$bdd->prepare("INSERT INTO omap_ocean(x1,xx1,y1,yy1,x2,xx2,y2,yy2,name,global,autor,creation) VALUES(:x1,:xx1,:y1,:yy1,:x2,:xx2,:y2,:yy2,:name,:global,:autor,NOW());");
							$reqSQL->execute(array(':x1'=>$c['x1'],':y1'=>$c['y1'],':x2'=>$c['x2'],':y2'=>$c['y2'],':xx1'=>$c['xx1'],':yy1'=>$c['yy1'],':xx2'=>$c['xx2'],':yy2'=>$c['yy2'],':name'=>$c['name'],':global'=>$c['gbl'],':autor'=>$_SESSION['id']));

							$reqSQL=$bdd->query("SELECT LAST_INSERT_ID();");
							$repSQL=$reqSQL->fetch();
							$datas['id']=$repSQL[0];

							$repSQL=$bdd->query("SELECT * FROM ocean WHERE id=".$repSQL[0].";");
							$sea=$repSQL->fetch();
							$datas['sea']['top']['x']=$sea['x1'];
							$datas['sea']['top']['y']=$sea['y1'];
							$datas['sea']['top']['xx']=$sea['xx1'];
							$datas['sea']['top']['yy']=$sea['yy1'];
							$datas['sea']['btm']['x']=$sea['x2'];
							$datas['sea']['btm']['y']=$sea['y2'];
							$datas['sea']['btm']['xx']=$sea['xx2'];
							$datas['sea']['btm']['yy']=$sea['yy2'];
							$datas['sea']['name']=$sea['name'];
							$datas['sea']['global']=nl2br(htmlspecialchars($sea['global']));

							$datas['error']='0';
						}else{
							$datas['error']='202';
							$datas['erdesc']='No integrer datas sent';
						}
					}else{
						$datas['error']='201';
						$datas['erdesc']='No numeric datas sent';
					}
				}else{
					$datas['error']='200';
					$datas['erdesc']='No datas sent';
				}
			break;
			case 'ni' :
				if(isset($_POST['x1']) AND isset($_POST['y1']) AND isset($_POST['x2']) AND isset($_POST['y2']) AND isset($_POST['xx1']) AND isset($_POST['yy1']) AND isset($_POST['xx2']) AND isset($_POST['yy2']) AND isset($_POST['name']) AND isset($_POST['gbl']) AND isset($_POST['id'])){
				$c=array('sea'=>$_POST['id'],'x1'=>$_POST['x1'],'y1'=>$_POST['y1'],'x2'=>$_POST['x2'],'y2'=>$_POST['y2'],'xx1'=>$_POST['xx1'],'yy1'=>$_POST['yy1'],'xx2'=>$_POST['xx2'],'yy2'=>$_POST['yy2']);
				$c['name']=htmlspecialchars($_POST['name']);
				$c['gbl']=htmlspecialchars($_POST['gbl']);
					if(is_numeric($c['sea']) AND is_numeric($c['x1']) AND is_numeric($c['y1']) AND is_numeric($c['x2']) AND is_numeric($c['y2']) AND is_numeric($c['xx1']) AND is_numeric($c['yy1']) AND is_numeric($c['xx2']) AND is_numeric($c['yy2'])){
						$c['sea']=get_numeric($c['sea']);
						$c['x1']=get_numeric($c['x1']);
						$c['y1']=get_numeric($c['y1']);
						$c['x2']=get_numeric($c['x2']);
						$c['y2']=get_numeric($c['y2']);
						$c['xx1']=get_numeric($c['xx1']);
						$c['yy1']=get_numeric($c['yy1']);
						$c['xx2']=get_numeric($c['xx2']);
						$c['yy2']=get_numeric($c['yy2']);
						if(is_int($c['x1']) AND is_int($c['y1']) AND is_int($c['x2']) AND is_int($c['y2']) AND is_int($c['xx1']) AND is_int($c['yy1']) AND is_int($c['xx2']) AND is_int($c['yy2'])){
							$reqSQL=$bdd->prepare("INSERT INTO omap_islands(sea,x1,xx1,y1,yy1,x2,xx2,y2,yy2,name,global,autor,creation) VALUES(:sea,:x1,:xx1,:y1,:yy1,:x2,:xx2,:y2,:yy2,:name,:global,:autor,NOW());");
							$reqSQL->execute(array(':sea'=>$c['sea'],':x1'=>$c['x1'],':y1'=>$c['y1'],':x2'=>$c['x2'],':y2'=>$c['y2'],':xx1'=>$c['xx1'],':yy1'=>$c['yy1'],':xx2'=>$c['xx2'],':yy2'=>$c['yy2'],':name'=>$c['name'],':global'=>$c['gbl'],':autor'=>$_SESSION['id']));

							$reqSQL=$bdd->query("SELECT LAST_INSERT_ID();");
							$repSQL=$reqSQL->fetch();
							$datas['id']=$repSQL[0];

							$repSQL=$bdd->query("SELECT * FROM omap_islands WHERE id=".$repSQL[0].";");
							$island=$repSQL->fetch();
							$datas['island']['top']['x']=$island['x1'];
							$datas['island']['top']['y']=$island['y1'];
							$datas['island']['top']['xx']=$island['xx1'];
							$datas['island']['top']['yy']=$island['yy1'];
							$datas['island']['btm']['x']=$island['x2'];
							$datas['island']['btm']['y']=$island['y2'];
							$datas['island']['btm']['xx']=$island['xx2'];
							$datas['island']['btm']['yy']=$island['yy2'];
							$datas['island']['name']=$island['name'];
							$datas['island']['sea']=$island['sea'];
							$datas['island']['global']=nl2br(htmlspecialchars($island['global']));	

							$datas['error']='0';
						}else{
							$datas['error']='202';
							$datas['erdesc']='No integrer datas sent';
						}
					}else{
						$datas['error']='201';
						$datas['erdesc']='No numeric datas sent';
					}
				}else{
					$datas['error']='200';
					$datas['erdesc']='No datas sent';
				}
			break;
			case 'nc' :
				if(isset($_POST['x']) AND isset($_POST['y'])){
					$repSQL=$bdd->query("SELECT cases FROM omap_islands WHERE id=".$_POST['id']);				
					$island=$repSQL->fetch();
					if($island['cases']==''){
						$repSQL=$bdd->query("SELECT x1,xx1,y1,yy1,x2,xx2,y2,yy2 FROM islands WHERE id=".$_POST['id']);	
						$island=$repSQL->fetch();
						$j= (18*$island['y2']+6*$island['yy2']) - (18*$island['y1']+6*$island['yy1']) + 6;
						$jj= (18*$island['x2']+6*$island['xx2']) - (18*$island['x1']+6*$island['xx1']) + 6;
						$cases=array();
						for($i=0;$i<$j;$i++){
							$cases[]=array();
							for($ii=0;$ii<$jj;$ii++){
								$cases[$i][]=0;
							}
						}
					}else{
						$cases=JSON_decode($island['cases']);
					}
					$lieu = $cases[$_POST['y']][$_POST['x']];
					$lieu=array('s'=>$_POST['s'],'c'=>JSON_decode($_POST['c']));
					
					$p=JSON_decode($_POST['p']);
					if($p[0] AND $_POST['x']<count($cases[0])){//droite
						if($cases[$_POST['y']][$_POST['x']+1]==0){
							$cases[$_POST['y']][$_POST['x']+1]=array('s'=>'nsp');
						}
						$lieu['pr']=1;
					}else{
						$lieu['pr']=0;
					}
					if($p[1] AND $_POST['y']<count($cases)){//bas
						if($cases[$_POST['y']+1][$_POST['x']]==0){
							$cases[$_POST['y']+1][$_POST['x']]=array('s'=>'nsp');
						}
						$lieu['pb']=1;
					}else{
						$lieu['pb']=0;
					}
					if($p[2] AND $_POST['y']>0){//haut
						if($cases[$_POST['y']-1][$_POST['x']]===0){
							$cases[$_POST['y']-1][$_POST['x']]=array('s'=>'nsp','pb'=>1);
						}else{
							$cases[$_POST['y']-1][$_POST['x']]->pr=1;
						}
					}
					if($p[3] AND $_POST['x']>0){//gauche
						if($cases[$_POST['y']][$_POST['x']-1]===0){
							$cases[$_POST['y']][$_POST['x']-1]=array('s'=>'nsp','pr'=>1);
						}else{
							$cases[$_POST['y']][$_POST['x']-1]->pr=1;
						}
					}
					$lieu['t']=time();
					$lieu['a']=$SESSION['id'];
					$lieu['goodinf']=$lieu['badinf']=0;
					$cases[$_POST['y']][$_POST['x']]=$lieu;
					$final=addslashes(JSON_encode($cases,8));
					$reqSQL=$bdd->query("UPDATE omap_islands SET cases='".$final."' WHERE id=".$_POST['id'].";");
					$datas['error']='0';
				}else{
					$datas['error']='200';
				}
			break;
			case 'se' :
				if(isset($_POST['id']) AND isset($_POST['name']) AND isset($_POST['gbl'])){
				
					$toset = array (
						'name' => addslashes($_POST['name']),
						'gbl' => addslashes($_POST['gbl'])
					);
				
					$reqSQL=$bdd->query("UPDATE omap_ocean SET global='".$toset['gbl']."', name='".$toset['name']."' WHERE id=".$_POST['id'].";");
					$repSQL=$bdd->query("SELECT id,name,global FROM omap_ocean WHERE id=".$_POST['id'].";");
					$sea=$repSQL->fetch();
					$datas['sea']['name']=htmlspecialchars($sea['name']);
					$datas['sea']['global']=nl2br(htmlspecialchars($sea['global']));
					$datas['sea']['id']=get_numeric($sea['id']);
					$datas['error']=0;
				}else{
					$datas['error']='200';
				}
			break;
			case 'ie' :
				if(isset($_POST['id']) AND isset($_POST['name']) AND isset($_POST['gbl'])){
				
					$toset = array (
						'name' => addslashes($_POST['name']),
						'gbl' => addslashes($_POST['gbl'])
					);
				
					$reqSQL=$bdd->query("UPDATE omap_islands SET global='".$toset['gbl']."', name='".$toset['name']."' WHERE id=".$_POST['id'].";");
					$repSQL=$bdd->query("SELECT id,name,global FROM omap_islands WHERE id=".$_POST['id'].";");
					$island=$repSQL->fetch();
					$datas['i']['name']=htmlspecialchars($island['name']);
					$datas['i']['global']=nl2br(htmlspecialchars($island['global']));
					$datas['i']['id']=get_numeric($island['id']);
					$datas['error']=0;
				}else{
					$datas['error']='200';
				}
			break;
			case 'ce' :
				if(isset($_POST['x']) AND isset($_POST['y'])){
					$repSQL=$bdd->query("SELECT cases FROM omap_islands WHERE id=".$_POST['id']);				
					$island=$repSQL->fetch();
					if($island['cases']==''){
						$repSQL=$bdd->query("SELECT x1,xx1,y1,yy1,x2,xx2,y2,yy2 FROM omap_islands WHERE id=".$_POST['id']);	
						$island=$repSQL->fetch();
						$j= (18*$island['y2']+6*$island['yy2']) - (18*$island['y1']+6*$island['yy1']) + 6;
						$jj= (18*$island['x2']+6*$island['xx2']) - (18*$island['x1']+6*$island['xx1']) + 6;
						$cases=array();
						for($i=0;$i<$j;$i++){
							$cases[]=array();
							for($ii=0;$ii<$jj;$ii++){
								$cases[$i][]=0;
							}
						}
					}else{
						$cases=JSON_decode($island['cases']);
					}
					$lieu = $cases[$_POST['y']][$_POST['x']];
					$lieu=array('s'=>$_POST['s'],'c'=>JSON_decode($_POST['c']));
					
					$p=JSON_decode($_POST['p']);
					if($p[0] AND $_POST['x']<count($cases[0])){//droite
						if($cases[$_POST['y']][$_POST['x']+1]==0){
							$cases[$_POST['y']][$_POST['x']+1]=array('s'=>'nsp');
						}
						$lieu['pr']=1;
					}else{
						$lieu['pr']=0;
					}
					if($p[1] AND $_POST['y']<count($cases)){//bas
						if($cases[$_POST['y']+1][$_POST['x']]==0){
							$cases[$_POST['y']+1][$_POST['x']]=array('s'=>'nsp');
						}
						$lieu['pb']=1;
					}else{
						$lieu['pb']=0;
					}
					if($p[2] AND $_POST['y']>0){//haut
						if($cases[$_POST['y']-1][$_POST['x']]===0){
							$cases[$_POST['y']-1][$_POST['x']]=array('s'=>'nsp','pb'=>1);
						}else{
							$cases[$_POST['y']-1][$_POST['x']]->pb=1;
						}
					}
					if($p[3] AND $_POST['x']>0){//gauche
						if($cases[$_POST['y']][$_POST['x']-1]===0){
							$cases[$_POST['y']][$_POST['x']-1]=array('s'=>'nsp','pr'=>1);
						}else{
							$cases[$_POST['y']][$_POST['x']-1]->pr=1;
						}
					}
					$lieu['t']=time();
					$lieu['a']=$_SESSION['id'];
					$lieu['goodinf']=$lieu['badinf']=0;
					$cases[$_POST['y']][$_POST['x']]=$lieu;
					$final=addslashes(JSON_encode($cases,8));
					$reqSQL=$bdd->query("UPDATE islands SET cases='".$final."' WHERE id=".$_POST['id'].";");
					$datas['error']='0';
				}else{
					$datas['error']='200';
				}
			break;
			case 'rms' :
				if(isset($_POST['id'])){
					$repSQL = $bdd->prepare("SELECT id,badinf,goodinf FROM omap_ocean WHERE id=:id;");
					$repSQL->execute(array(':id' =>$_POST['id']));
					if($repSQL->rowCount()>=1){
						$sea=$repSQL->fetch();
						if($sea['goodinf']==0 OR ($sea['badinf']>0)){
							$repSQL=$bdd->prepare("DELETE FROM ocean WHERE id=:id;");
							$repSQL->execute(array(':id' => $_POST['id']));
							$repSQL=$bdd->prepare("DELETE FROM islands WHERE sea=:id");
							$repSQL->execute(array(':id' => $_POST['id']));
							$datas['error']=0;
							$datas['id']=$_POST['id'];
						}else{
							$datas['error']=300;
							$datas['erdesc']='Trop de votes positifs.';
						}
					}else{
						$datas['error']=200;
						$datas['erdesc']='Mer inexistante';
					}
				}else{
					$datas['error']='200';
				}
			break;
			case 'rmi' :
				if(isset($_POST['id'])){
					$repSQL = $bdd->prepare("SELECT id,badinf,goodinf FROM omap_islands WHERE id=:id;");
					$repSQL->execute(array(':id' =>$_POST['id']));
					if($repSQL->rowCount()>=1){
						$island=$repSQL->fetch();
						if($island['goodinf']==0 OR ($island['badinf']>0)){
							$repSQL=$bdd->prepare("DELETE FROM islands WHERE id=:id");
							$repSQL->execute(array(':id' => $_POST['id']));
						}
					}
					$datas['error']=0;
					$datas['id']=$_POST['id'];
				}else{
					$datas['error']=201;
					$datas['erdesc']='Missing id';
				}
			break;
			case 'rmc' :
				if(isset($_POST['x']) AND isset($_POST['y'])){
					$repSQL=$bdd->query("SELECT cases FROM omap_islands WHERE id=".$_POST['id']);				
					$island=$repSQL->fetch();
					if($island['cases']!=''){
						$cases=JSON_decode($island['cases']);
						$cases[$_POST['y']][$_POST['x']]=0;
						if($_POST['y']>0 AND gettype($cases[$_POST['y']-1][$_POST['x']])=='object'){//haut
							$cases[$_POST['y']-1][$_POST['x']]->pb=0;
						}
						if($_POST['x']>0 AND gettype($cases[$_POST['y']][$_POST['x']-1])=='object'){//haut
							$cases[$_POST['y']][$_POST['x']-1]->pr=0;
						}
						$final=addslashes(JSON_encode($cases,8));
						$reqSQL=$bdd->query("UPDATE omap_islands SET cases='".$final."' WHERE id=".$_POST['id'].";");
						$datas['error']=0;
						$datas['id']=$_POST['id'];
						$datas['x']=$_POST['x'];
						$datas['y']=$_POST['y'];
					}else{
						//error : ile d�j� vide
					}
				}else{
					$datas['error']='200';
				}
			break;
			default :
			$datas['error']='101';
			$datas['erdesc']='Unknown sub-action';
		}
	}else{
		$datas['error']='301';
		$datas['erdesc']='Right "edit" is required';
	}

}elseif(isset($_POST['rateomap'])){
	if(isset($_SESSION['id'])){
		switch($_POST['rateomap']){
			case 'c':
				if(isset($_POST['rate']) AND isset($_POST['id']) AND isset($_POST['x']) AND isset($_POST['y'])){
					$coord['y']=get_numeric($_POST['y']);
					$coord['x']=get_numeric($_POST['x']);
					$repSQL=$bdd->prepare("SELECT cases FROM omap_islands WHERE id=:id;");
					$repSQL->execute(array(':id'=>$_POST['id']));
					$island=$repSQL->fetch();
					if($island['cases']==''){
						$datas['error']='200';
						break;
					}else{
						$cases=JSON_decode($island['cases']);
					}
					if(!is_numeric($cases[$_POST['y']][$_POST['x']])){
						$lieu = get_object_vars($cases[$_POST['y']][$_POST['x']]);
						$update=false;
						if($_POST['rate']=='true'){
							if(!array_key_exists('gv',$lieu)){
								$lieu['gv']=array();}
							if(!array_key_exists('goodinf',$lieu)){
								$lieu['goodinf']=0;}
							if(!in_array($_SESSION['id'],$lieu['gv'])){
								$lieu['gv'][]=$_SESSION['id'];
								$lieu['goodinf']=get_numeric($lieu['goodinf'])+1;
								if(array_key_exists('bv',$lieu) AND in_array($_SESSION['id'],$lieu['bv'])){
									unset($lieu['bv'][array_search($_SESSION['id'],$lieu['bv'])]);
									$lieu['badinf']=get_numeric($lieu['badinf'])-1;
								}
								$update=true;
							}
						}else{
							if(!array_key_exists('bv',$lieu)){
								$lieu['bv']=array();}
							if(!array_key_exists('badinf',$lieu)){
								$lieu['badinf']=0;}
							if(!in_array($_SESSION['id'],$lieu['bv'])){
								$lieu['bv'][]=$_SESSION['id'];
								$lieu['badinf']=get_numeric($lieu['badinf'])+1;
								if(array_key_exists('bv',$lieu) AND in_array($_SESSION['id'],$lieu['gv'])){
									unset($lieu['gv'][array_search($_SESSION['id'],$lieu['gv'])]);
									$lieu['goodinf']=get_numeric($lieu['goodinf'])-1;
								}
								$update=true;
							}
						}

							$cases[$_POST['y']][$_POST['x']]=$lieu;
							$repSQL=$bdd->prepare("UPDATE omap_islands SET cases=:cases WHERE id=:id;");
							$repSQL->execute(array(':cases'=>JSON_encode($cases,8),':id'=>$_POST['id']));
							$datas['error']='0';


					}else{
						$datas['error']='200';
						break;
					}
				}else{
						$datas['error']='200';
						break;
				}
			break;
			case 'i':
				if(isset($_POST['rate']) AND isset($_POST['id'])){
					$repSQL=$bdd->prepare("SELECT bv,gv,badinf,goodinf FROM omap_islands WHERE id=:id;");
					$repSQL->execute(array(':id'=>$_POST['id']));
					$island=$repSQL->fetch();
					$island['gv']=JSON_decode($island['gv']);
					$island['bv']=JSON_decode($island['bv']);
					if(!is_array($island['gv'])){$island['gv']=array();};
					if(!is_array($island['bv'])){$island['bv']=array();};
					$update=false;
					if($_POST['rate']=='true'){
						if(!in_array($_SESSION['id'],$island['gv'])){
							$island['gv'][]=get_numeric($_SESSION['id']);
							
							if(in_array($_SESSION['id'],$island['bv'])){
								unset($island['bv'][array_search($_SESSION['id'],$island['bv'])]);
							}
							$update=true;
						}
					}else{
						if(!in_array($_SESSION['id'],$island['bv'])){
							$island['bv'][]=get_numeric($_SESSION['id']);
							if(in_array($_SESSION['id'],$island['gv'])){
								unset($island['gv'][array_search($_SESSION['id'],$island['gv'])]);
							}
							$update=true;
						}
					}
					if($update){
							$island['goodinf']=count($island['gv']);
							$island['badinf']=count($island['bv']);
							$repSQL=$bdd->prepare("UPDATE omap_islands SET bv=:bv,gv=:gv,badinf=:badinf,goodinf=:goodinf WHERE id=:id;");
							$repSQL->execute(array(':bv'=>JSON_encode($island['bv'],8),':gv'=>JSON_encode($island['gv'],8),':badinf'=>$island['badinf'],':goodinf'=>$island['goodinf'],':id'=>$_POST['id']));
							$datas['error']='0';
					}else{
						$datas['error']='2001';
						break;
					}
				}else{
					$datas['error']='2002';
					break;
				}
			break;
			case 's':
				if(isset($_POST['rate']) AND isset($_POST['id'])){
					$repSQL=$bdd->prepare("SELECT bv,gv,badinf,goodinf FROM omap_ocean WHERE id=:id;");
					$repSQL->execute(array(':id'=>$_POST['id']));
					$sea=$repSQL->fetch();
					$sea['gv']=JSON_decode($sea['gv']);
					$sea['bv']=JSON_decode($sea['bv']);
					if(!is_array($sea['gv'])){$sea['gv']=array();};
					if(!is_array($sea['bv'])){$sea['bv']=array();};
					$update=false;
					if($_POST['rate']=='true'){
						if(!in_array($_SESSION['id'],$sea['gv'])){
							$sea['gv'][]=get_numeric($_SESSION['id']);
							
							if(in_array($_SESSION['id'],$sea['bv'])){
								unset($sea['bv'][array_search($_SESSION['id'],$sea['bv'])]);
							}
							$update=true;
						}
					}else{
						if(!in_array($_SESSION['id'],$sea['bv'])){
							$sea['bv'][]=get_numeric($_SESSION['id']);
							if(in_array($_SESSION['id'],$sea['gv'])){
								unset($sea['gv'][array_search($_SESSION['id'],$sea['gv'])]);
							}
							$update=true;
						}
					}
					if($update){
							$sea['goodinf']=count($sea['gv']);
							$sea['badinf']=count($sea['bv']);
							$repSQL=$bdd->prepare("UPDATE omap_ocean SET bv=:bv,gv=:gv,badinf=:badinf,goodinf=:goodinf WHERE id=:id;");
							$repSQL->execute(array(':bv'=>JSON_encode($sea['bv'],8),':gv'=>JSON_encode($sea['gv'],8),':badinf'=>$sea['badinf'],':goodinf'=>$sea['goodinf'],':id'=>$_POST['id']));
							$datas['error']='0';
					}else{
						$datas['error']='2001';
						break;
					}
				}else{
					$datas['error']='2002';
					break;
				}
			break;
			default:
									$datas['error']='203';
						break;
		}
	}else{
		$datas['error']='300';
		$datas['erdesc']='Login is required';
	}
}elseif(isset($_POST['setuser'])){
	if(isset($_SESSION['id'])){
		switch($_POST['setuser']){
			case 'pos':
				if(isset($_POST['elem']) AND isset($_POST['id'])){
					$repSQL=$bdd->prepare("SELECT ".'part'.$_SESSION['part']." FROM omap_users WHERE id=:id;");
					$repSQL->execute(array(':id'=>$_SESSION['id']));
					$user=$repSQL->fetch();
					$part=$user[0];
					if($part==''){
						$part=array('pos','islands'=>array(),'seas'=>array());
					}else{
						$part=get_object_vars(JSON_decode($part));
					}
					
					$part['pos']->type=$_POST['elem'];
					$part['pos']->id=get_numeric($_POST['id']);

					$repSQL=$bdd->prepare("UPDATE users SET part".$_SESSION['part']."=:part WHERE id=:id;");
					$repSQL->execute(array(':part'=>JSON_encode($part,8),':id'=>$_SESSION['id']));
					$datas['pos']['type']=$_POST['elem'];
					$datas['pos']['id']=get_numeric($_POST['id']);
					$datas['error']='0';

				}else{
					$datas['error']='200';
				}
			break;
			case 'completion':
				if(isset($_POST['elem']) AND isset($_POST['id']) AND isset($_POST['st'])){
					$repSQL=$bdd->prepare("SELECT ".'part'.$_SESSION['part']." FROM users WHERE id=:id;");
					$repSQL->execute(array(':id'=>$_SESSION['id']));
					$user=$repSQL->fetch();
					$part=$user[0];
					if($part==''){
						$part=array('pos'=>'','islands'=>array(),'seas'=>array());
					}else{
						$part=get_object_vars(JSON_decode($part));
					}
					switch($_POST['elem']){
						case'i':
							if(isset($part['islands'])){
								foreach ($part['islands'] as $island) {
									if($island->id == $_POST['id']){
										unset($island);
									}
								}
							}else{
								$part['islands']=array();
							}
							$part['islands'][]=array('id'=>get_numeric($_POST['id']),'st'=>get_numeric($_POST['st']));
							$repSQL=$bdd->prepare("UPDATE omap_users SET part".$_SESSION['part']."=:part WHERE id=:id;");
							$repSQL->execute(array(':part'=>JSON_encode($part,8),':id'=>$_SESSION['id']));
							$datas['ct']='i';
							$datas['i']['st']=get_numeric($_POST['st']);
							$datas['i']['id']=get_numeric($_POST['id']);
							$datas['error']='0';
						break;
						case's':
							if(isset($part['seas'])){
								foreach ($part['seas'] as $sea) {
									if($sea->id == $_POST['id']){
										unset($sea);
									}
								}
							}else{
								$part['seas']=array();
							}
							$part['seas'][]=array('id'=>get_numeric($_POST['id']),'st'=>get_numeric($_POST['st']));
							$repSQL=$bdd->prepare("UPDATE omap_users SET part".$_SESSION['part']."=:part WHERE id=:id;");
							$repSQL->execute(array(':part'=>JSON_encode($part,8),':id'=>$_SESSION['id']));
							$datas['ct']='s';
							$datas['s']['st']=get_numeric($_POST['st']);
							$datas['s']['id']=get_numeric($_POST['id']);
							$datas['error']='0';
						break;
						default:
						$datas['error']='200';
					}
				}else{
					$datas['error']='200';
					$datas['erdesc']='Missing parameters';
				}
			break;			
			default:
				$datas['error']='200';
				$datas['erdesc']='Incorrect request for action "setuser"';
		}
	}else{
		$datas['error']='300';
		$datas['erdesc']='Login is required';
	}
}else{
	$datas['error']='100';
	$datas['erdesc']='Unknown action';
}
echo JSON_encode($datas); ?>